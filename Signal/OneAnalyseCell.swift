//
//  OneAnalyseCell.swift
//  Signal
//
//  Created by Sina Khalili on 2/21/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class OneAnalyseCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: SignalLabel!
    @IBOutlet weak var lblDescription: SignalLabel!
    @IBOutlet var lblCategory: SignalLabel!
    @IBOutlet var lblType: SignalLabel!
    @IBOutlet var lblSymbol: SignalLabel!
    @IBOutlet var imgSymbol: SignalImageView!
    @IBOutlet var lblDateTime: UILabel!
}
