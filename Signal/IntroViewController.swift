//
//  IntroViewController.swift
//  Signal
//
//  Created by Sina Khalili on 2/17/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class IntroViewController: SignalViewController {

    @IBOutlet weak var txtEmail: SignalTextField!

    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = true
    }
    override func viewDidLoad() {
        // so back item text will be empty...
        self.title = " "        
    }

    @IBAction func loginPressed(sender: AnyObject) {
        self.performSegueWithIdentifier("login", sender: self)
    }

    @IBAction func registerPressed(sender: AnyObject) {
        // register button pressed
        resignFirstResponder()
        register()
    }
    override func submit() {
        // register from keyboard
        register()
    }
    
    func register() {
        // perform register view controller
        performSegueWithIdentifier("register", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "register" {
            (segue.destinationViewController as! RegisterViewController).txtEmail.text = txtEmail.text
        }
        if segue.identifier == "login" {
            (segue.destinationViewController as! LoginViewController).txtEmail.text = txtEmail.text
        }
    }
}
