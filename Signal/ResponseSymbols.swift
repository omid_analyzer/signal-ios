//
//  ResponseSymbols.swift
//  Signal
//
//  Created by Sina Khalili on 3/1/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Arrow

struct ResponseSymbols: ArrowParsable {
    
    var data                = [OneSymbol]()
    
    init() {
        
    }
    init(json: JSON) {
        var dataArray = NSArray()
        dataArray       <-- json["symbols"]
        if(dataArray.count>0) {
            for i in 0...dataArray.count-1 {
                data.append(OneSymbol(json: dataArray[i]))
            }
        }
    }
    
}

/////

struct OneSymbol: ArrowParsable {
    
    var id                  = Int()
    var tseId               = String()
    var isin                = String()
    var name                = String()
    var image               = String()
    var deletedAt           = NSDate()
    var createdAt           = NSDate()
    var updatedAt           = NSDate()
    
    init() {
        
    }
    init(json: JSON) {
        
        Arrow.setDateFormat("yyyy-MM-dd HH:mm:ss")
        
        id              <-- json["id"]
        tseId           <-- json["tse_id"]
        isin            <-- json["isin"]
        name            <-- json["name"]
        image           <-- json["image"]
        deletedAt       <-- json["deleted_at"]
        createdAt       <-- json["created_at"]
        updatedAt       <-- json["updated_at"]

    }
    
    func imageURL() -> String {
        return "\(URLs.UPLOADS)\(image)"
    }
    
}