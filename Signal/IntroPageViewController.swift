//
//  IntroPageViewController.swift
//  Signal
//
//  Created by Sina Khalili on 2/18/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class IntroPageViewController: UIPageViewController, UIPageViewControllerDataSource {

    let ITEMS_COUNT = 3
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        createPageViewController()
        setupPageControl()
    }
    
    func reload() {
        createPageViewController()
        setupPageControl()
    }
    
    private func createPageViewController() {
        
        dataSource = self
        let firstController = getItemController(0)!
        let startingViewControllers: NSArray = [firstController]
        setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)

    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.backgroundColor = UIColor.groupTableViewBackgroundColor()
        appearance.pageIndicatorTintColor = UIColor.lightGrayColor()
        appearance.currentPageIndicatorTintColor = self.view.tintColor
    }
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! IntroItemViewController

        if itemController.itemIndex > 0 {
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! IntroItemViewController
        
        if itemController.itemIndex+1 < ITEMS_COUNT {
            return getItemController(itemController.itemIndex+1)
        }
        
        return nil
    }
    
    private func getItemController(itemIndex: Int) -> UIViewController? {
        
        if itemIndex == 0 {
            let pageVideoItemController = self.storyboard!.instantiateViewControllerWithIdentifier("introVideoItem") as! IntroItemViewController
            
            pageVideoItemController.itemIndex = itemIndex
            
            return pageVideoItemController

        } else if itemIndex < ITEMS_COUNT {
            let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("introItem") as! IntroItemViewController

            pageItemController.itemIndex = itemIndex
            
            return pageItemController
        }
        
        return nil
    }
    
    // MARK: - Page Indicator
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return ITEMS_COUNT
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }

}
