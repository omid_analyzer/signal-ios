//
//  TabBarController.swift
//  Signal
//
//  Created by Sina Khalili on 2/18/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        
        self.delegate = self
        
        // customizing tabBar fonts
        let attributes = [NSFontAttributeName:UIFont(name: Constants.FONT, size: 10)!]
        for tab in  self.tabBar.items!
        {
            tab.setTitleTextAttributes(attributes, forState: .Normal)
        }
        
        let appDelegate     = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let responseInit    = appDelegate.responseInit
        let appLastIds      = AppLastIds()
        
        if responseInit.summary.unreadTutorials > 0 && appLastIds.lastTutorialId > 0 {
            self.tabBar.items![0].badgeValue = responseInit.summary.unreadTutorials.persianDigits()
        }
        if responseInit.summary.unreadAnalyses > 0 && appLastIds.lastAnalyseId > 0 {
            self.tabBar.items![1].badgeValue = responseInit.summary.unreadAnalyses.persianDigits()
        }
        if responseInit.summary.unreadSignals > 0 && appLastIds.lastSignalId > 0 {
            self.tabBar.items![2].badgeValue = responseInit.summary.unreadSignals.persianDigits()
        }
        
    }
    
    var prevViewController = UIViewController()
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        if prevViewController == viewController {
            if viewController.respondsToSelector(Selector("scrollToTop:")) {
                viewController.performSelector(Selector("scrollToTop:"), withObject: self)
            }
        } else {
            prevViewController = viewController
        }
    }
    
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        item.badgeValue = nil
    }
    
}
