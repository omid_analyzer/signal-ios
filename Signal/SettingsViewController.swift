//
//  SettingsViewController.swift
//  Signal
//
//  Created by Sina Khalili on 3/17/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import Alamofire

class SettingsViewController: SignalViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let ROW_DETAILS = 0, ROW_NEWS = 1, ROW_LOGOUT = 2
    
    override func viewDidLoad() {

        initRefreshControl(tableView, color: UIColor.whiteColor())
        self.tableView.separatorInset       = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.tableFooterView      = UIView()
        self.tableView.rowHeight            = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight   = 240

        updateItems()
        
        downloadData()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBar.hidden = false
    }
    
    override func refresh(sender: AnyObject) {
        isRefreshing = true
        Alamofire.Manager.cancelRequests(URLs.INITIALIZE)
        downloadData()
    }
    
    override func endRefreshing() {
        super.endRefreshing()
    }
    
    func updateItems() {
        tableView.reloadData()
    }
    
    func downloadData() {
        
        WebServices.initialize(self, view: self.view, success: { (response) -> () in
            
            // set global login info
            let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            appDelegate.responseInit    = ResponseInit(json: response!)
            
            self.updateItems()
            
        })
        
    }
    
    func scrollToTop(sender: AnyObject) {
        self.tableView.setContentOffset(CGPoint.zero, animated:true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("titleCell", forIndexPath: indexPath) as! TitleCell
            cell.lblTitle.text = Strings.PROFILE_SETTINGS_APP
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("notifsCell", forIndexPath: indexPath) as! SwitchCell
            return cell
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("titleCell", forIndexPath: indexPath) as! TitleCell
            cell.lblTitle.text = Strings.MORE
            return cell
        case 3:
            let cell = tableView.dequeueReusableCellWithIdentifier("detailDisclosureCell", forIndexPath: indexPath) as! DetailDisclosureCell
            cell.lblTitle.text = Strings.PROFILE_SETTINGS_CONTACT_SUPPORT
            return cell
        case 4:
            let cell = tableView.dequeueReusableCellWithIdentifier("detailDisclosureCell", forIndexPath: indexPath) as! DetailDisclosureCell
            cell.lblTitle.text = Strings.PROFILE_SETTINGS_ABOUT_OMID_ANALYSER
            return cell
        case 5:
            let cell = tableView.dequeueReusableCellWithIdentifier("detailCell", forIndexPath: indexPath) as! DetailCell
            cell.lblTitle.text  = Strings.APP_VERSION
            cell.lblDetail.text = Strings.AppVersion
            return cell
        default:
            break
        }
        
        return UITableViewCell()
        
    }
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
}
