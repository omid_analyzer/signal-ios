//
//  InitViewController.swift
//  Signal
//
//  Created by Sina Khalili on 3/16/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import Pantry

class InitViewController: SignalViewController {
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if let loginInfo: LoginInfo = Pantry.unpack(Constants.PANTRY_LOGIN_INFO) {
            
            let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            appDelegate.loginInfo       = loginInfo
            
            // refresh token if it's expired
            if let isValid: Bool = Pantry.unpack(Constants.PANTRY_LOGIN_INFO_IS_VALID) {
                if isValid {
                    refresh(self)
                } else {
                    RefreshToken.refresh(false)
                }
            } else {
                RefreshToken.refresh(false)
            }
            
        } else {
            // not logged in
            self.performSegueWithIdentifier("intro", sender: self)
        }
        
    }
    
    override func refresh(sender: AnyObject) {
        super.refresh(sender)
        
        WebServices.initialize(self, view: self.view, success: { (response) -> () in
            
            if response != nil {
                
                let responseInit = ResponseInit(json: response!)
                self.initializeBy(responseInit)
                
                // cache init
                if let json = (response as! NSDictionary).toString() {
                    Pantry.pack(json, key: Constants.PANTRY_CHACHED_INIT)
                }
                
            } else {
                
                self.view.messageError(Strings.ERROR_NO_RESPONSE)
                
            }
            
            }, failure: { (code, response) -> () in
                // error
                if(response==nil || !ResponseErrorProcessor.process(response!, view: self.view)) {
                    self.view.messageWarning("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                }
            }, startFailure: { (error) -> () in
                if error.code == .NotConnectedToInternet {
                    if let cachedInit: String = Pantry.unpack(Constants.PANTRY_CHACHED_INIT) {
                        if let dictionary = cachedInit.toDictionaryAsAnyObject() {
                            let responseInit = ResponseInit(json: dictionary)
                            if !responseInit.forceInit {
                                self.initializeBy(responseInit)
                            }
                        }
                    }
                }
        })
    }
    
    func initializeBy(responseInit: ResponseInit) {
        // set global login info
        let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        appDelegate.responseInit    = responseInit
        
        self.performSegueWithIdentifier("main", sender: self)
    }
    
}
