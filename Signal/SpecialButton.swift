//
//  SpecialButton.swift
//  Signal
//
//  Created by Sina Khalili on 3/1/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class SpecialButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayer()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupLayer()
    }
    
    func setupLayer() {
        // Changing Font
        titleLabel?.font = UIFont(name: Constants.FONT, size: titleLabel!.font.pointSize)
        // Rounded Edge
        layer.cornerRadius = 3
        clipsToBounds = true
        addBorder(Colors.specialBlue)
        set(false)
    }

    func addBorder(color: UIColor) {
        layer.borderWidth = 1
        layer.borderColor = color.CGColor
    }

    func set(set: Bool) {
        if set {
            // Background Color
            backgroundColor = Colors.specialBlue
            // Text Color
            setTitleColor(UIColor.whiteColor(), forState: .Normal)
        } else {
            // Background Color
            backgroundColor = UIColor.clearColor()
            // Text Color
            setTitleColor(Colors.specialBlue, forState: .Normal)
        }
    }
}
