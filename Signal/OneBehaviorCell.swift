//
//  OneBehaviorCell.swift
//  Signal
//
//  Created by Sina Khalili on 3/1/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class OneBehaviorCell: UITableViewCell {

    @IBOutlet var desc: UILabel!
    @IBOutlet var time: SignalLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
