//
//  WSBuySignal.swift
//  Signal
//
//  Created by Sina Khalili on 3/17/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Alamofire

extension WebServices {
    
    static func buySignal(signalId: Int, viewController: SignalViewController, view: UIView,
        success: Success? = nil, failure: Failure? = nil, startFailure: StartFailure? = nil) {
            
            GeneralFunctions.showProgress()

            // check token validation
            if !viewController.isTokenValid() {
                GeneralFunctions.hideProgress()
                startFailure?(error: AppInternalError(code: .InvalidToken))
                return
            }
            
            // checking connection
            if(!Reachability.isConnectedToNetwork()) {
                GeneralFunctions.hideProgress()
                view.messageWarning(Strings.NOT_CONNECTED_TO_INTERNET)
                startFailure?(error: AppInternalError(code: .NotConnectedToInternet))
                return
            }
            
            // calling web service
            
            let params = ["signal_id":  String(signalId)
                , "access token":       (UIApplication.sharedApplication().delegate as! AppDelegate).loginInfo.accessToken]
            
            Alamofire.request(.POST, URLs.BUY_SIGNAL, parameters: params)
                .responseJSON { response in
                    GeneralFunctions.hideProgress()
                    if let res = response.response {
                        if res.statusCode == 200 {
                            success?(response: response.result.value)
                        } else {
                            failure?(code: res.statusCode, response: response.result.value)
                        }
                    } else {
                        failure?(code: Constants.NoResponseCode, response: Constants.NoResponseJSON)
                    }
            }
            
    }
    
}