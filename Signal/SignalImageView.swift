//
//  SignalImageView.swift
//  Signal
//
//  Created by Sina Khalili on 3/1/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class SignalImageView: UIImageView {

    func makeRoundedCorners(cornerRadius: CGFloat = 10) {
        // rounded corner
        self.layer.cornerRadius = cornerRadius
        clipsToBounds = true
        
        //        layer.borderWidth = 0.1
        //        layer.borderColor = UIColor.whiteColor().CGColor
    }

}
