//
//  URLs.swift
//  Signal
//
//  Created by Sina Khalili on 2/19/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Foundation

class URLs {
    
    static let SITE                 = "http://app.omidanalyzer.com/"
    static let API                  = "\(SITE)application/api/v2/"
    static let UPLOADS              = "\(SITE)uploads/"

    static let INITIALIZE           = "\(API)initialize"
    static let GET_CATEGORIES       = "\(API)categories"

    static let REGISTER             = "\(API)register"
    static let ACCESS_TOKEN         = "\(SITE)auth/access_token"
    
    static let GET_TUTORIALS        = "\(API)tutorial/list"
    static let GET_ANALYZES         = "\(API)analyse/list"

    static let GET_SIGNALS          = "\(API)signal/list"
    static let GET_BEHAVIORS        = "\(API)signal/behavior"
    static let GET_SIGNAL_DETAILS   = "\(API)signal/detail"
    static let FOLLOW_SIGNAL        = "\(API)signal/follow"
    static let UNFOLLOW_SIGNAL      = "\(API)signal/unFollow"
    static let CHART_SIGNAL         = "\(API)chart/signal"
    static let BUY_SIGNAL           = "\(API)signal/buy"

    static let GET_USER_DETAILS     = "\(API)user/detail"

}