//
//  SignalDetailsViewController.swift
//  Signal
//
//  Created by Sina Khalili on 3/2/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import Alamofire

class SignalDetailsViewController: SignalViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var imgSymbol: SignalImageView!
    @IBOutlet var lblTitle: SignalLabel!
    @IBOutlet var lblDateTime: UILabel!
    @IBOutlet var lblSymbol: SignalLabel!
    @IBOutlet var lblRisk: SignalLabel!
    @IBOutlet var lblPowers: SignalLabel!
    
    @IBOutlet var topBarBackground: UIImageView!
    @IBOutlet var listHeaderBackground: UIImageView!
    @IBOutlet var listHeaderBackgroundY: NSLayoutConstraint!
    @IBOutlet var listHeaderBackgroundHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var signal      = OneSignal()
    var isLoaded    = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barStyle = .Black
        
        initRefreshControl(tableView, color: UIColor.whiteColor())
        self.tableView.separatorStyle       = .None
        self.tableView.rowHeight            = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight   = 120
        scrollViewDidScroll(tableView)
        
        if signal.symbol.image.characters.count > 0 {
            imgSymbol.load(signal.symbol.imageURL(), placeholder: UIImage(named: "Logo"), completionHandler: nil)
        } else {
            imgSymbol.image = UIImage(named: "Logo")
        }
        imgSymbol.makeRoundedCorners()
        
        updateItems()
        
        downloadData()
        
    }
    
    override func refresh(sender: AnyObject) {
        isRefreshing = true
        Alamofire.Manager.cancelRequests(URLs.GET_SIGNAL_DETAILS)
        downloadData()
    }
    
    override func endRefreshing() {
        super.endRefreshing()
        indicator.stopAnimating()
    }
    
    func updateItems() {
        lblTitle.text       = signal.title
        lblSymbol.text      = signal.symbol.name
        lblDateTime.text    = signal.createdAt.persianDateTime()
        lblRisk.text        = signal.risk.persianValue()
        if isLoaded {
            lblPowers.text      = signal.powers.persianDigits()
        }

        tableView.reloadData()
    }
    
    func downloadData() {
        
        // check if received
        if let received = DBSignals().getItem(self.signal.id) {
            self.signal = received
            self.isLoaded   = true
            self.updateItems()
        }
        
        WebServices.getSignalDetails(signal.id, viewController: self, view: tableView, success: { (response) -> () in
            
            // response
            self.endRefreshing()
            self.endLoadingMore()
            if (response != nil) {
            self.signal     = OneSignal(json: response!)
            self.isLoaded   = true
            self.updateItems()
            }
            
            }, failure: { (code, response) in
                // error
                self.endRefreshing()
                self.endLoadingMore()
                if(response==nil || !ResponseErrorProcessor.process(response!, view: self.tableView)) {
                    self.tableView.messageError("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                }
            }, startFailure: { (error) -> () in
                if error.code == .NotConnectedToInternet {
                    if let received = DBSignals().getItem(self.signal.id) {
                        self.endRefreshing()
                        self.endLoadingMore()
                        self.signal = received
                        self.isLoaded   = true
                        self.updateItems()
                    }
                }
        })
    }
    
    func scrollToTop(sender: AnyObject) {
        self.tableView.setContentOffset(CGPoint.zero, animated:true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoaded {
            return 4
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("titleCell", forIndexPath: indexPath) as! TitleCell
            
            cell.lblTitle.text          = Strings.SIGNAL_DETAILS
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("detailsCell", forIndexPath: indexPath) as! SignalDetailsCell
            
            cell.lblTMin.text           = signal.tmin.persianDigits()
            cell.lblTMax.text           = signal.tmax.persianDigits()
            
            cell.lblGoal.text           = signal.goal.persianDigits()
            cell.lblLose.text           = signal.lose.persianDigits()
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("titleCell", forIndexPath: indexPath) as! TitleCell
            
            cell.lblTitle.text          = Strings.DESCRIPTION
            
            return cell
        case 3:
            let cell = tableView.dequeueReusableCellWithIdentifier("descriptionCell", forIndexPath: indexPath) as! DescriptionCell
            
            cell.lblDescription.text    = signal.content
            
            return cell
        default:
            break
        }
        
        return UITableViewCell()
        
    }
    
    // topBar
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        if yOffset <= 0 {
            listHeaderBackgroundY.constant          = -20
            listHeaderBackgroundHeight.constant     = 252 - yOffset
            topBarBackground.image                  = nil
        } else {
            listHeaderBackgroundY.constant          = -20 - yOffset
            listHeaderBackgroundHeight.constant     = 252
            topBarBackground.image                  = UIImage(named: Constants.Image_Lists_Header_TopBar)
        }
    }
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
}
