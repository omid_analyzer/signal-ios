//
//  ResponseAnalyzes.swift
//  Signal
//
//  Created by Sina Khalili on 2/21/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Arrow

class ResponseAnalyzes: ArrowParsable {
    
    var total               = Int()
    var perPage             = Int()
    var currentPage         = Int()
    var lastPage            = Int()
    var nextPageUrl         = String()
    var prevPageUrl         = String()
    var from                = Int()
    var to                  = Int()
    var data                = [OneAnalyze]()
    
    init() {
        
    }
    required init(json: JSON) {
        total           <-- json["total"]
        perPage         <-- json["per_page"]
        currentPage     <-- json["current_page"]
        lastPage        <-- json["last_page"]
        nextPageUrl     <-- json["next_page_url"]
        prevPageUrl     <-- json["prev_page_url"]
        from            <-- json["from"]
        to              <-- json["to"]
        var dataArray = NSArray()
        dataArray       <-- json["data"]
        if(dataArray.count>0) {
            for i in 0...dataArray.count-1 {
                data.append(OneAnalyze(json: dataArray[i]))
            }
        }
    }
    
}

/////

struct OneAnalyze: ArrowParsable {
    
    var id                  = Int()
    var categoryId          = Int()
    var symbolId            = Int()
    var title               = String()
    var summary             = String()
    var type                = String()
    var createdAt           = NSDate()
    var isAccessible        = Bool()
    var category            = OneCategory()
    var symbol              = OneSymbol()
    
    init(json: JSON) {
        
        Arrow.setDateFormat("yyyy-MM-dd HH:mm:ss")
        
        id              <-- json["id"]
        categoryId      <-- json["category_id"]
        symbolId        <-- json["symbol_id"]
        title           <-- json["title"]
        summary         <-- json["summary"]
        type            <-- json["type"]
        createdAt       <-- json["created_at"]
        isAccessible    <-- json["isAccessible"]
        category        <== json["category"]
        symbol          <== json["symbol"]
    }
    
}