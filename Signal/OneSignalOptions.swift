//
//  OneSignalOptions.swift
//  Signal
//
//  Created by Sina Khalili on 3/17/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import Alamofire

extension SignalsViewController {

    func followTap(recognizer: UITapGestureRecognizer) {
        let oneItem = items[recognizer.view!.tag]
        if oneItem.isFollowInProgress {
            return
        }
        items[recognizer.view!.tag].isFollowInProgress = true
        tableView.reloadData()
        if oneItem.isFollowing {
            unfollowSignal(oneItem)
        } else {
            followSignal(oneItem)
        }
    }
    
    func followSignal(signal: OneSignal) {
        WebServices.followSignal(signal, viewController: self, view: tableView, success: { (response) -> () in
            self.setFollowing(signal.id, isFollowing: true)
            self.tableView.reloadData()
            }, failure: { (code, response) -> () in
                self.setFollowInProgress(signal.id, isFollowInProgress: false)
                self.tableView.reloadData()
                if(response==nil || !ResponseErrorProcessor.process(response!, view: self.tableView)) {
                    self.tableView.messageError("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                }
        })
    }
    
    func unfollowSignal(signal: OneSignal) {
        WebServices.unfollowSignal(signal, viewController: self, view: tableView, success: { (response) -> () in
            self.setFollowing(signal.id, isFollowing: false)
            self.tableView.reloadData()
            }, failure: { (code, response) -> () in
                self.setFollowInProgress(signal.id, isFollowInProgress: false)
                self.tableView.reloadData()
                if(response==nil || !ResponseErrorProcessor.process(response!, view: self.tableView)) {
                    self.tableView.messageError("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                }
        })
    }
    
    /**
     Set follow status of specific signal
     
     - parameter signalId:    The Id of signal
     - parameter isFollowing: The status to be set
     */
    func setFollowing(signalId: Int, isFollowing: Bool) {
        for i in 0...items.count {
            if items[i].id == signalId {
                items[i].isFollowing        = isFollowing
                items[i].isFollowInProgress = false
                break
            }
        }
        for i in 0...allItems.count {
            if allItems[i].id == signalId {
                allItems[i].isFollowing     = isFollowing
                items[i].isFollowInProgress = false
                break
            }
        }
    }
    func setFollowInProgress(signalId: Int, isFollowInProgress: Bool) {
        for i in 0...items.count {
            if items[i].id == signalId {
                items[i].isFollowInProgress = isFollowInProgress
                break
            }
        }
        for i in 0...allItems.count {
            if allItems[i].id == signalId {
                items[i].isFollowInProgress = isFollowInProgress
                break
            }
        }
    }
    
    func buyTap(recognizer: UITapGestureRecognizer) {
        let oneItem = items[recognizer.view!.tag]
        if oneItem.isAccessible {
            if oneItem.isReceived {
                // delete signal
                
                self.db.deleteItem(oneItem.id)
                self.setReceived(oneItem.id, isReceived: false)
                self.tableView.reloadData()
            } else {
                // receive signal
                
                GeneralFunctions.showProgress()
                WebServices.getSignalDetails(oneItem.id, viewController: self, view: tableView, success: { (response) -> () in

                    if (response != nil) {
                        self.db.saveItem(OneSignal(json: response!))
                        self.setReceived(oneItem.id, isReceived: true)
                        self.tableView.reloadData()
                        GeneralFunctions.showText("\(Strings.SIGNAL) \(Strings.RECEIVED)", delay: 3000)
                    } else {
                        
                    }

                    }, failure: { (code, response) -> () in
                        GeneralFunctions.hideProgress()
                    }, startFailure: { (error) -> () in
                        GeneralFunctions.hideProgress()
                })
                
            }
        } else {
            WebServices.buySignal(oneItem.id, viewController: self, view: self.tableView, success: { (response) -> () in
                self.setAccessible(oneItem.id, isAccessible: true)
                self.tableView.reloadData()
                }, failure: { (code, response) -> () in
                    // error
                    if(response==nil || !ResponseErrorProcessor.process(response!, view: self.view)) {
                        self.tableView.messageError("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                    }
            })
        }
    }
    
    /**
     Set accessible status of specific signal
     
     - parameter signalId:    The Id of signal
     - parameter isAccessible: The status to be set
     */
    func setAccessible(signalId: Int, isAccessible: Bool) {
        for i in 0...items.count {
            if items[i].id == signalId {
                items[i].isAccessible        = isAccessible
                break
            }
        }
        for i in 0...allItems.count {
            if allItems[i].id == signalId {
                allItems[i].isAccessible     = isAccessible
                break
            }
        }
    }

    /**
     Set received status of specific signal
     
     - parameter signalId:    The Id of signal
     - parameter isReceived: The status to be set
     */
    func setReceived(signalId: Int, isReceived: Bool) {
        for i in 0...items.count {
            if items[i].id == signalId {
                items[i].isReceived         = isReceived
                break
            }
        }
        for i in 0...allItems.count {
            if allItems[i].id == signalId {
                allItems[i].isReceived      = isReceived
                break
            }
        }
    }

}