//
//  CategoriesViewController.swift
//  Signal
//
//  Created by Sina Khalili on 3/4/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import Alamofire

class CategoriesViewController: SignalViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!

    @IBOutlet var indicator: UIActivityIndicatorView!
    
    var delegate        = SignalViewController()
    var categoryType    = CategoryType.signal
    var items           = [OneCategory]()

    var categoryId: Int?
    var selectedIndex   = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        initWhiteNavigationBar()
        let attributes = [NSFontAttributeName:UIFont(name: Constants.FONT_BOLD, size: 15)!]
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(attributes, forState: .Normal)

        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)

        initRefreshControl(tableView)
        
        if categoryId > 0 {
            selectedIndex = -1
        }

        updateItems()
        
        indicator.startAnimating()
        downloadData()
        
    }
    
    override func refresh(sender: AnyObject) {
        super.refresh(self)

        hideProgress()
        Alamofire.Manager.cancelRequests(URLs.GET_CATEGORIES)
        downloadData()
    }
    
    override func endRefreshing() {
        super.endRefreshing()
        
        indicator.hidden = true
        indicator.stopAnimating()
    }

    func updateItems() {
        tableView.reloadData()
    }
    
    func downloadData() {
        WebServices.getCategories(categoryType, viewController: self, view: tableView, success: { (response) -> () in
            
            // response
            self.endRefreshing()
            if response != nil {
                self.items = ResponseCategories(json: response!).data
            }
            if self.categoryId > 0 {
                for (index, element) in self.items.enumerate() {
                    if element.id == self.categoryId {
                        self.selectedIndex = index + 1
                    }
                }
            }
            self.updateItems()
            
            }, failure: { (code, response) in
                
                // error
                super.endRefreshing()
                if(response==nil || !ResponseErrorProcessor.process(response!, view: self.tableView)) {
                    self.tableView.messageError("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                }
        })
    }
    
    @IBAction func closePressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("categoryCell", forIndexPath: indexPath) as! CategoryCell
        
        if indexPath.row > 0 {
        let oneItem             = items[indexPath.row-1]
            cell.lblName.text       = oneItem.title
            cell.img.image          = UIImage(named: "Icon_Categories_Item")
        } else {
            cell.lblName.text       = "\(Strings.ALL_OF)\(categoryType.persianValue())\(Strings.PERSIAN_S)"
            cell.img.image          = UIImage(named: "Icon_Categories_All")
        }

        if selectedIndex == indexPath.row {
            cell.lblName.textColor  = Colors.darkBlue
            cell.imgCheck.hidden    = false
        } else {
            cell.lblName.textColor  = UIColor.blackColor()
            cell.imgCheck.hidden    = true
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedIndex = indexPath.row
        updateItems()
        var categoryId: Int?
        if selectedIndex > 0 {
            categoryId = items[selectedIndex-1].id
        } else {
            categoryId = nil
        }
        switch categoryType {
        case .tutorial:
            (delegate as! TutorialsViewController).categoryId       = categoryId
            (delegate as! TutorialsViewController).refreshNeeded    = true
            break
        case .analyse:
            (delegate as! AnalysesViewController).categoryId        = categoryId
            (delegate as! AnalysesViewController).refreshNeeded     = true
            break
        case .signal:
            (delegate as! SignalsViewController).categoryId         = categoryId
            (delegate as! SignalsViewController).refreshNeeded      = true
            break
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
}
