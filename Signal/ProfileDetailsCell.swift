//
//  ProfileDetailsCell.swift
//  Signal
//
//  Created by Sina Khalili on 3/16/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class ProfileDetailsCell: UITableViewCell {
 
    @IBOutlet var viewContainer: SignalContainerView!
    @IBOutlet var lblBalance: UILabel!
    @IBOutlet var lblDaysToExpire: UILabel!
    
    @IBOutlet var btnAccountExpire: UIButton!
}