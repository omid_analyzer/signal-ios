//
//  AnalysesViewController.swift
//  Signal
//
//  Created by Sina Khalili on 2/21/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import Alamofire
import Pantry

class AnalysesViewController: SignalViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var categoryId: Int?
    
    var items: [OneAnalyze]     = [OneAnalyze]()
    var lastResponse            = ResponseAnalyzes()
    
    override func viewDidLoad() {
        
        navigationController?.navigationBar.barStyle = .Black
        
        initRefreshControl(self.tableView)
        
        if let chachedResponse: String = Pantry.unpack(Constants.PANTRY_CHACHED_ANALYSES) {
            // load cache
            if let json = chachedResponse.toDictionaryAsAnyObject() {
                self.lastResponse = ResponseAnalyzes(json: json)
                self.items.appendContentsOf(self.lastResponse.data)
                self.updateItems()
                refreshNeeded = true
            } else {
                downloadData(1)
            }
        } else {
            downloadData(1)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBar.hidden = true
    }
    
    var refreshNeeded = false
    override func viewDidAppear(animated: Bool) {
        if refreshNeeded {
            refreshNeeded = false
            tableView.setContentOffset(CGPointMake(0, -100), animated: true)
            self.refreshControl.beginRefreshing()
            refresh(self)
        }
    }
    
    override func refresh(sender: AnyObject) {
        super.refresh(self)
        Alamofire.Manager.cancelRequests(URLs.GET_ANALYZES)
        downloadData(1)
    }
    
    override func endRefreshing() {
        super.endRefreshing()
        Alamofire.Manager.cancelRequests(URLs.GET_ANALYZES)
    }
    override func endLoadingMore() {
        super.endLoadingMore()
        self.indicator.hidden = true
        self.indicator.stopAnimating()
        self.isLoadingMore = false
        Alamofire.Manager.cancelRequests(URLs.GET_ANALYZES)
    }
    
    func updateItems() {
        tableView.reloadData()
    }
    
    func downloadData(page: Int) {
        WebServices.getAnalyzes(page, categoryId: categoryId, viewController: self, indicator: indicator, view: tableView, success: { (response) -> () in
            
            // response
            super.endRefreshing()
            super.endLoadingMore()
            if response != nil {
                self.lastResponse = ResponseAnalyzes(json: response!)
                if page==1 {
                    self.items.removeAll()
                    // cache response
                    if self.categoryId == nil {
                        if let json = (response as! NSDictionary).toString() {
                            Pantry.pack(json, key: Constants.PANTRY_CHACHED_ANALYSES)
                        }
                    }
                }
                self.items.appendContentsOf(self.lastResponse.data)
                self.updateItems()
                var appLastIds = AppLastIds()
                if self.items.count > 0 && appLastIds.lastAnalyseId < self.items[0].id {
                    appLastIds.lastAnalyseId = self.items[0].id
                    appLastIds.save()
                }
            }
            
            }, failure: { (code, response) in
                
                // error
                super.endRefreshing()
                super.endLoadingMore()
                if(response==nil || !ResponseErrorProcessor.process(response!, view: self.tableView)) {
                    self.tableView.messageError("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                }
        })
    }
    
    func scrollToTop(sender: AnyObject) {
        self.tableView.setContentOffset(CGPoint.zero, animated:true)
    }
    
    // table view implementation
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if items.count == 0 && !isRefreshing {
            return 1
        }
        return items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // nothing cell!
        if indexPath.row == 0 && items.count == 0 {
            return tableView.dequeueReusableCellWithIdentifier("nothingCell", forIndexPath: indexPath)
        }

        let cell = tableView.dequeueReusableCellWithIdentifier("oneAnalyseCell", forIndexPath: indexPath) as! OneAnalyseCell
        
        // showing signal
        let oneItem                 = items[indexPath.row]
        cell.lblTitle.text          = oneItem.title
        cell.lblDescription.text    = oneItem.summary
        cell.lblCategory.text       = oneItem.category.title
        cell.lblType.text           = oneItem.type
        cell.lblDateTime.text       = oneItem.createdAt.persianDateTime()
        if oneItem.symbol.image.characters.count > 0 {
            cell.imgSymbol.load(oneItem.symbol.imageURL(), placeholder: UIImage(named: "Logo"), completionHandler: nil)
        } else {
            cell.imgSymbol.image = UIImage(named: "Logo")
        }
        cell.imgSymbol.makeRoundedCorners()
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let row = indexPath.row
        
        if row>0 && row>items.count-4 {
            loadMoreIfPossible()
        }
        
    }
    
    func loadMoreIfPossible() {
        if !isRefreshing && !isLoadingMore && lastResponse.lastPage>lastResponse.currentPage {
            self.isLoadingMore = true
            downloadData(lastResponse.currentPage+1)
        }
    }
    
    @IBAction func categoriesPressed(sender: AnyObject) {
        ViewPresenters.openCategories(self, categoryType: .analyse, categoryId: categoryId)
    }
}
