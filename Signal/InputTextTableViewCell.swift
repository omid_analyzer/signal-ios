//
//  InputTextTableViewCell.swift
//  Signal
//
//  Created by Sina Khalili on 2/18/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class InputTextTableViewCell: UITableViewCell {

    @IBOutlet weak var label: SignalLabel!
    @IBOutlet weak var textField: SignalTextField!

}
