//
//  IntroItemViewController.swift
//  Signal
//
//  Created by Sina Khalili on 2/18/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class IntroItemViewController: UIViewController {
    
    var itemIndex = 0
    
    @IBOutlet weak var introImage: UIImageView!
    @IBOutlet weak var introTitle: UILabel!
    @IBOutlet weak var introLabel: UILabel!
    
    @IBOutlet weak var introImageHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch itemIndex {
        case 0:
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "playIntroVideo:")
            introImage.addGestureRecognizer(tapGestureRecognizer)
            introImage.userInteractionEnabled = true
        case 1:
            introImageHeight.constant = 0
            introTitle.text = "تحلیلگر بورس امید"
            introLabel.text = "با توجه به اینکه بیکسووایزر به هسته ی معاملات از طریق زیرساخت Bixo دسترسی مستقیم دارد به سرعت میتواند بازار را اسکن و سیگنال ها را با حدأکثر جزییات به کاربران ارسال کند."
            break
            
        case 2:
            introImage.image = UIImage(imageLiteral: "Intro_Slides_3")
            introTitle.text = "تصمین سیگنال ها"
            introLabel.text = "ما سود ده بودن سیگنال ها را برای شما تضمین میکنیم. به این صورت که اگر برای سیگنالی حد ضرر فعال شد, هزینه ی سیگنال به حساب شما بازگردانده خواهد شد."
            break
            
        default:
            break
        }
    }
    
    func playIntroVideo(sender: UITapGestureRecognizer) {
        do {
            try playVideo()
        } catch AppError.InvalidResource(let name, let type) {
            debugPrint("Could not find resource \(name).\(type)")
        } catch {
            debugPrint("Generic error")
        }
    }
    
    private func playVideo() throws {
        guard let path = NSBundle.mainBundle().pathForResource("Intro_Video", ofType:"mp4") else {
            throw AppError.InvalidResource("Intro_Video", "mp4")
        }
        let player = AVPlayer(URL: NSURL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.presentViewController(playerController, animated: true) {
            player.play()
        }
    }
    
    enum AppError : ErrorType {
        case InvalidResource(String, String)
    }
    
}
