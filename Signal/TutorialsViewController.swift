//
//  TutorialsViewController.swift
//  Signal
//
//  Created by Sina Khalili on 2/21/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import Alamofire
import Pantry

class TutorialsViewController: SignalViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var items: [OneTutorial]        = [OneTutorial]()
    var lastResponse                = ResponseTutorials()
    
    var categoryId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barStyle = .Black
        navigationController?.navigationBar.hidden = true
        
        initRefreshControl(self.tableView)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight    = 244
        
        if let chachedResponse: String = Pantry.unpack(Constants.PANTRY_CHACHED_TUTORIALS) {
            // load cache
            if let json = chachedResponse.toDictionaryAsAnyObject() {
                self.lastResponse = ResponseTutorials(json: json)
                self.items.appendContentsOf(self.lastResponse.data)
                self.updateItems()
                refreshNeeded = true
            } else {
                downloadData(1)
            }
        } else {
            downloadData(1)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBar.hidden = true
    }
    
    var refreshNeeded = false
    override func viewDidAppear(animated: Bool) {
        if refreshNeeded {
            refreshNeeded = false
            tableView.setContentOffset(CGPointMake(0, -100), animated: true)
            self.refreshControl.beginRefreshing()
            refresh(self)
        }
    }
    
    override func refresh(sender: AnyObject) {
        super.refresh(self)
        Alamofire.Manager.cancelRequests(URLs.GET_TUTORIALS)
        downloadData(1)
    }
    
    override func endRefreshing() {
        super.endRefreshing()
        Alamofire.Manager.cancelRequests(URLs.GET_TUTORIALS)
    }
    override func endLoadingMore() {
        super.endLoadingMore()
        self.indicator.hidden = true
        self.indicator.stopAnimating()
        self.isLoadingMore = false
        Alamofire.Manager.cancelRequests(URLs.GET_TUTORIALS)
    }
    
    @IBAction func segmentChanged(sender: AnyObject) {
        updateItems()
    }
    
    func updateItems() {
        tableView.reloadData()
    }
    
    func downloadData(page: Int) {
        WebServices.getTutorials(page, categoryId: categoryId, viewController: self, indicator: indicator, view: tableView, success: { (response) -> () in
            
            // response
            self.endRefreshing()
            self.endLoadingMore()
            if response != nil {
                self.lastResponse = ResponseTutorials(json: response!)
                if page==1 {
                    self.items.removeAll()
                    // cache response
                    if self.categoryId == nil {
                        if let json = (response as! NSDictionary).toString() {
                            Pantry.pack(json, key: Constants.PANTRY_CHACHED_TUTORIALS)
                        }
                    }
                }
                var appLastIds = AppLastIds()
                if self.items.count > 0 && appLastIds.lastTutorialId < self.items[0].id {
                    appLastIds.lastTutorialId = self.items[0].id
                    appLastIds.save()
                }
            }
            self.items.appendContentsOf(self.lastResponse.data)
            self.updateItems()
            
            }, failure: { (code, response) in
                
                // error
                super.endRefreshing()
                super.endLoadingMore()
                if(response==nil || !ResponseErrorProcessor.process(response!, view: self.tableView)) {
                    self.tableView.messageError("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                }
        })
    }
    
    func scrollToTop(sender: AnyObject) {
        self.tableView.setContentOffset(CGPoint.zero, animated:true)
    }
    
    // table view implementation
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if items.count == 0 && !isRefreshing {
            return 1
        }
        return items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // nothing cell!
        if indexPath.row == 0 && items.count == 0 {
            return tableView.dequeueReusableCellWithIdentifier("nothingCell", forIndexPath: indexPath)
        }

        let cell = tableView.dequeueReusableCellWithIdentifier("oneTutorialCell", forIndexPath: indexPath) as! OneTutorialCell
        
        // showing signal
        let oneItem                 = items[indexPath.row]
        cell.viewContainer.setupLayer(2, shadowRadius: 2)
        cell.viewBehindImg.setupLayer(4, shadowRadius: 4)
        cell.img.makeRoundedCorners(4)
        cell.lblTitle.text          = oneItem.title
        cell.lblDescription.text    = oneItem.summary
        cell.lblCategory.text       = oneItem.category.title
        cell.lblDateTime.text       = oneItem.createdAt.persianDateTime()
        cell.btnBuy.setTitle(oneItem.price.formattedPrice(), forState: .Normal)
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let row = indexPath.row
        
        if row>0 && row>items.count-4 {
            loadMoreIfPossible()
        }
        
    }
    
    func loadMoreIfPossible() {
        if !isRefreshing && !isLoadingMore && lastResponse.lastPage>lastResponse.currentPage {
            self.isLoadingMore = true
            downloadData(lastResponse.currentPage+1)
        }
    }
    
    @IBAction func categoriesPressed(sender: AnyObject) {
        ViewPresenters.openCategories(self, categoryType: .tutorial, categoryId: categoryId)
    }
    @IBAction func downloadsPressed(sender: AnyObject) {
    }
    
}
