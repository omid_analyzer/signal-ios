//
//  Strings.swift
//  Signal
//
//  Created by Sina Khalili on 2/19/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Foundation

class Strings {
    
    static let AppVersion                           = "۰.۴"
    
    static let Check                                = "✓دنبال شده"

    static let PLEASE_WAIT                          = "لطفأ کمی صبر کنید"
    static let NOT_CONNECTED_TO_INTERNET            = "اتصال به اینترنت برقرار نمیباشد."

    static let ERROR_CODE                           = "کد خطا:"
    
    static let ERROR_IN_CONNECTION                  = "خطا در اتصال به سرور"
    static let ERROR_NO_RESPONSE                    = "پاسخی دریافت نشد."
    static let ERROR_EMAIL_TAKEN                    = "ایمیل تکراری است."
    static let ERROR_EMAIL_INVALID                  = "ایمیل معتبر نمیباشد."
    static let ERROR_NAME_INVALID                   = "نام قابل قبول نمیباشد."
    static let ERROR_MOBILE_INVALID                 = "شماره قابل قبول نمیباشد."
    static let ERROR_PASSWORD_INVALID               = "رمز عبور قابل قبول نمیباشد."
    static let ERROR_PASSWORD_SHORT                 = "رمز عبور کوتاه میباشد."
    static let ERROR_PRESENTER_EMAIL_INVALID        = "ایمیل معرف به درستی وارد نشده است."
    static let ERROR_AUTH_EXCEPTION                 = "اطلاعات وارد شده صحیح نمیباشد."
    static let ERROR_PRESENTER_EMAIL_NOT_FOUND      = "ایمیل معرف یافت نشد."
    static let ERROR_USER_NOT_FOUND                 = "کاربری با این ایمیل یافت نشد."
    static let ERROR_UNIQUE_ID_INVALID              = "شناسه ی دستگاه معبتر نمیباشد."
    static let ERROR_UNKNOWN                        = "خطای غیر منتظره ای رخ داد."
    static let ERROR_BALANCE_NOT_ENOUGH             = "موجودی کافی نیست."
    static let ERROR_PAYMENT_VERIFIED_BEFORE        = "پرداخت قبلأ اعمال شده است."
    static let ERROR_PAYMENT                        = "پرداخت انجام نشد."
    static let ERROR_PRODUCT_NOT_FOUND              = "محصول یافت نشد."
    static let ERROR_PLATFORM_LIMIT                 = "حساب به محدودیت تعداد دستگاه رسیده است."
    static let ERROR_INTERNAL                       = "خطا در سیستم، لطفأ مجددأ تلاش کنید."
    static let ERROR_NOT_PAID_BEFORE                = "این محتوا قبلأ خریداری نشده است."
    static let ERROR_DUPLICATE_REQUEST              = "درخواست ارسالی تکراری است."
    static let ERROR_SIGNAL_OUT_OF_SELL             = "سیگنال به محدودیت خرید رسیده است و قابل خرید نمیباشد."
    static let ERROR_SIGNAL_ID_INVALID              = "شناسه ی سینگال صحیح نمیباشد."
    static let ERROR_ANALYSE_ID_INVALID             = "شناسه ی تحلیل صحیح نمیباشد."
    static let ERROR_TUTORIAL_ID_INVALID            = "شناسه ی آموزش صحیح نمیباشد."
    static let ERROR_IN_PARAMETERS                  = "خطا در پارامتر های ارسالی به سرور"
    static let ERROR_KEY_INVALID                    = "کلید جستجو صحیح نمیباشد."
    static let ERROR_SUBSCRIPTION_PLANE_ID_INVALID  = "شناسه ی پکیج کاربر طلایی صحیح نمیباشد."
    static let ERROR_PRODUCT_ID_INVALID             = "شناسه ی محصول صحیح نمیباشد."
    static let ERROR_PAYMENT_TOKEN_INVALID          = "پرداخت معتبر نمیباشد."
    static let ERROR_AMOUNT_INVALID                 = "مبلغ وارد شده صحیح نمیباشد."
    static let ERROR_DESCRIPTION_INVALID            = "توضیحات وارد شده صحیح نمیباشد."
    static let ERROR_AUTHORITY_INVALID              = "شناسه ی پرداخت معتبر نمیباشد."
    static let ERROR_STATUS_INVALID                 = "وضعیت پرداخت صحیح نمیباشد."

    static let ENTER_NAME                           = "نام را وارد نمایید."
    static let ENTER_EMAIL                          = "ایمیل را وارد نمایید."
    static let ENTER_PASSWORD                       = "رمز عبور را وارد نمایید."
    
    static let FREE                                 = "رایگان"
    static let SIGNAL_LIMIT_REACHED                 = "محدودیت به حد نصاب رسیده است."
    static let SIGNAL_BROKEN                        = "مبلغ به حساب شما بازگردانده شد."
    
    static let RISK_HIGH                            = "زیاد"
    static let RISK_MEDIUM                          = "متوسط"
    static let RISK_LOW                             = "کم"
    
    static let SIGNAL_DETAILS                       = "جزئیات سیگنال"
    static let DESCRIPTION                          = "توضیحات"
    
    static let NO_SIGNAL_BEHAVIORS                  = "رفتاری یافت نشد"

    static let ALL_OF                               = "همه ی "
    static let CATEGORY_TUTORIAL                    = "آموزش"
    static let CATEGORY_ANALYSE                     = "تحلیل"
    static let CATEGORY_SIGNAL                      = "سیگنال"
    static let PERSIAN_S                            = " ها"
    static let MORE                                 = "بیشتر"
    
    static let FOLLOW                               = "دنبال کنید"
    
    static let PROFILE_SETTINGS_APP                 = "تنظیمات برنامه"
    static let PROFILE_SETTINGS_CONTACT_SUPPORT     = "ارتباط با پشتیبانی"
    static let PROFILE_SETTINGS_ABOUT_OMID_ANALYSER = "درباره تحلیلگر امید"
    static let APP_VERSION                          = "نسخه برنامه"
    
    static let TUTORIAL                             = "آموزش"
    static let ANALYSE                              = "تحلیل"
    static let SIGNAL                               = "سیگنال"
    static let RECEIVE                              = "دریافت"
    static let RECEIVED                             = "دریافت شد"
    static let DELETE                               = "حذف"

}