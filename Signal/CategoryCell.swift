//
//  CategoryCell.swift
//  Signal
//
//  Created by Sina Khalili on 3/4/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet var img: UIImageView!
    @IBOutlet var lblName: SignalLabel!
    @IBOutlet var imgCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
