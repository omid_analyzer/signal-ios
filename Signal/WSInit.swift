//
//  WSInit.swift
//  Signal
//
//  Created by Sina Khalili on 3/16/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Foundation
import Alamofire
import Pantry

extension WebServices {
    
    static func initialize(viewController: SignalViewController, view: UIView,
        success: Success? = nil, failure: Failure? = nil, startFailure: StartFailure? = nil) {
            
            // check token validation
            if !viewController.isTokenValid() {
                startFailure?(error: AppInternalError(code: .InvalidToken))
                return
            }
            
            // checking connection
            if(!Reachability.isConnectedToNetwork()) {
                view.messageWarning(Strings.NOT_CONNECTED_TO_INTERNET)
                viewController.endRefreshing()
                viewController.endLoadingMore()
                startFailure?(error: AppInternalError(code: .NotConnectedToInternet))
                return
            }
            
            // calling web service
            
            let appLastIds  = AppLastIds()
            
            let params = ["access token": (UIApplication.sharedApplication().delegate as! AppDelegate).loginInfo.accessToken
                            , "last_signal_id":     String(appLastIds.lastSignalId)
                            , "last_analyse_id":    String(appLastIds.lastAnalyseId)
                            , "last_tutorial_id":   String(appLastIds.lastTutorialId)
                            , "last_news_id":       String(appLastIds.lastNewsId)]
            
            Alamofire.request(.POST, URLs.INITIALIZE, parameters: params)
                .responseJSON { response in
                    if let res = response.response {
                        if res.statusCode == 200 {
                            success?(response: response.result.value)
                        } else {
                            failure?(code: res.statusCode, response: response.result.value)
                        }
                    } else {
                        failure?(code: Constants.NoResponseCode, response: Constants.NoResponseJSON)
                    }
            }
            
    }
    
}