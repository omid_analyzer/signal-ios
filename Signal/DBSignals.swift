//
//  DBSignals.swift
//  Signal
//
//  Created by Sina Khalili on 3/18/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import SQLite

class DBSignals {
    
    var db: Connection
    var table: Table
    let id = Expression<Int>("id")
    let json = Expression<String>("json")
    
    /**
     Initializes the table if needed
     
     - returns: Database ready to be used
     */
    init() {
        let docsDir = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).last!
        let dbPath = (docsDir as NSString).stringByAppendingPathComponent("signals.sqlite")
        db = try! Connection(dbPath)
        db.trace { print($0) }
        
        table = Table("signals")
        
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(json)
            })
    }
    
    func saveItem(item: OneSignal) {
        let alice = table.filter(id == item.id)
        do {
            try db.run(alice.delete())
        } catch {
            
        }
        let insert = table.insert(id <- item.id, json <- item.json)
        try! db.run(insert)
        print("Saved signal: \(item.id)\nTotal signals: \(db.scalar(table.count))")
    }
    func deleteItem(itemId: Int) {
        let alice = table.filter(id == itemId)
        do {
            try db.run(alice.delete())
        } catch {
            
        }
    }
    
    /**
     Get items from table
     
     - returns: Items
     */
    func getItems() -> [OneSignal] {
        var items = [OneSignal]()
        do {
            for item in try db.prepare(table) {
                if let json = item[self.json].toDictionaryAsAnyObject() {
                    items.append(OneSignal(json: json))
                }
            }
        }catch let error as NSError {
            print(error)
        }
        return items
    }
    
    func getItem(itemId: Int) -> OneSignal? {
        let alice = table.filter(id == itemId)
        do {
            for item in try db.prepare(alice) {
                if let json = item[self.json].toDictionaryAsAnyObject() {
                    return OneSignal(json: json)
                }
            }
        }catch let error as NSError {
            print(error)
        }
        return nil
    }
    
    /**
     Check if something exists in db or not
     
     - parameter itemId: Item Id
     
     - returns: Exists or not
     */
    func exists(itemId: Int) -> Bool {
        let alice = table.filter(id == itemId)
        if db.scalar(alice.count) > 0 {
            return true
        } else {
            return false
        }
    }
    
}
