//
//  WSGetSignalsChart.swift
//  Signal
//
//  Created by Sina Khalili on 3/17/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftValidator
import Pantry

extension WebServices {
    
    static func getSignalsChart(viewController: SignalViewController, view: UIView,
        success: Success? = nil, failure: Failure? = nil, startFailure: StartFailure? = nil) {
            
            // check token validation
            if !viewController.isTokenValid() {
                startFailure?(error: AppInternalError(code: .InvalidToken))
                return
            }
            
            // checking connection
            if(!Reachability.isConnectedToNetwork()) {
                view.messageWarning(Strings.NOT_CONNECTED_TO_INTERNET)
                viewController.endRefreshing()
                viewController.endLoadingMore()
                startFailure?(error: AppInternalError(code: .NotConnectedToInternet))
                return
            }
            
            // calling web service
            
            let params = ["access_token":       (UIApplication.sharedApplication().delegate as! AppDelegate).loginInfo.accessToken]
            
            Alamofire.request(.GET, URLs.CHART_SIGNAL, parameters: params)
                .responseJSON { response in
                    if let res = response.response {
                        if res.statusCode == 200 {
                            success?(response: response.result.value)
                        } else {
                            failure?(code: res.statusCode, response: response.result.value)
                        }
                    } else {
                        failure?(code: Constants.NoResponseCode, response: Constants.NoResponseJSON)
                    }
            }
            
    }
    
}