//
//  AppLastIds.swift
//  Signal
//
//  Created by Sina Khalili on 3/16/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Foundation
import Pantry

struct AppLastIds: Storable {
    
    var lastSignalId    = 0
    var lastAnalyseId   = 0
    var lastTutorialId  = 0
    var lastNewsId      = 0

    init() {
        if let lastIds: AppLastIds = Pantry.unpack(Constants.PANTRY_LAST_IDS) {
            self = lastIds
        }
    }
    
    func save() {
        Pantry.pack(self, key: Constants.PANTRY_LAST_IDS)
    }
    
    init(warehouse: JSONWarehouse) {
        self.lastSignalId       = warehouse.get("last_signal_id")!
        self.lastAnalyseId      = warehouse.get("last_analyse_id")!
        self.lastTutorialId     = warehouse.get("last_tutorial_id")!
        self.lastNewsId         = warehouse.get("last_news_id")!
    }
    
    
    func toDictionary() -> [String : AnyObject] {
        return [    "last_signal_id":           lastSignalId
            ,       "last_analyse_id":          lastAnalyseId
            ,       "last_tutorial_id":         lastTutorialId
            ,       "last_news_id":             lastNewsId    ]
    }

}