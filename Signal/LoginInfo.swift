//
//  LoginInfo.swift
//  Signal
//
//  Created by Sina Khalili on 2/19/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Arrow
import Pantry

struct LoginInfo:ArrowParsable, Storable {

    var username            = String()
    var accessToken         = String()
    var tokenType           = String()
    var expiresIn           = Int()
    var refreshToken        = String()

    init() {
        
    }

    init(username: String, json: JSON) {
        self                = LoginInfo(json: json)
        self.username       = username
    }

    init(json: JSON) {
        accessToken         <-- json["access_token"]
        tokenType           <-- json["token_type"]
        expiresIn           <-- json["expires_in"]
        refreshToken        <-- json["refresh_token"]
    }

    init(warehouse: JSONWarehouse) {
        self.username       = warehouse.get("username")!
        self.accessToken    = warehouse.get("access_token")!
        self.tokenType      = warehouse.get("token_type")!
        self.expiresIn      = warehouse.get("expires_in")!
        self.refreshToken   = warehouse.get("refresh_token")!
    }
    
    
    func toDictionary() -> [String : AnyObject] {
        return [    "username":         username
                ,   "access_token":     accessToken
                ,   "token_type":       tokenType
                ,   "expires_in":       expiresIn
                ,   "refresh_token":    refreshToken    ]
    }
}