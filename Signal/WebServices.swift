//
//  WebServices.swift
//  Signal
//
//  Created by Sina Khalili on 3/3/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Alamofire

class WebServices {
    internal typealias Success              = ((response: AnyObject?) -> ())
    internal typealias Failure              = ((code: Int, response: AnyObject?) -> ())
    internal typealias StartFailure         = ((error: AppInternalError) -> ())
}

extension Alamofire.Manager
{
    private static func cancelTasksByUrl(tasks: [NSURLSessionTask], url: String)
    {
        for task in tasks
        {
            let hasPrefix = task.currentRequest?.URL?.description.hasPrefix(url)
            if hasPrefix != nil && hasPrefix! == true
            {
                task.cancel()
            }
        }
    }
    
    static func cancelRequests(url: String)
    {
        self.sharedInstance.session.getTasksWithCompletionHandler
            {
                (dataTasks, uploadTasks, downloadTasks) -> Void in
                
                self.cancelTasksByUrl(dataTasks     as [NSURLSessionTask], url: url)
                self.cancelTasksByUrl(uploadTasks   as [NSURLSessionTask], url: url)
                self.cancelTasksByUrl(downloadTasks as [NSURLSessionTask], url: url)
        }
    }
}