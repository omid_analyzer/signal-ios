//
//  ResponseSignals.swift
//  Signal
//
//  Created by Sina Khalili on 2/19/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Arrow

struct ResponseSignals: ArrowParsable {

    var total               = Int()
    var perPage             = Int()
    var currentPage         = Int()
    var lastPage            = Int()
    var nextPageUrl         = String()
    var prevPageUrl         = String()
    var from                = Int()
    var to                  = Int()
    var data                = [OneSignal]()

    init() {
        
    }
    init(json: JSON) {
        total           <-- json["total"]
        perPage         <-- json["per_page"]
        currentPage     <-- json["current_page"]
        lastPage        <-- json["last_page"]
        nextPageUrl     <-- json["next_page_url"]
        prevPageUrl     <-- json["prev_page_url"]
        from            <-- json["from"]
        to              <-- json["to"]
        var dataArray = NSArray()
        dataArray       <-- json["data"]
        if(dataArray.count>0) {
            for i in 0...dataArray.count-1 {
                data.append(OneSignal(json: dataArray[i]))
            }
        }
    }

}

/////

struct OneSignal: ArrowParsable {
    
    var json                = String()

    var id                  = Int()
    var symbolId            = Int()
    var risk                = SignalRisks.medium
    var categoryId          = Int()
    var title               = String()
    var summary             = String()
    var price               = Int()
    var limit               = Int()
    var sellCounter         = Int()
    var isBroken            = Int()
    var createdAt           = NSDate()
    var isAccessible        = Bool()
    var isFollowing         = Bool()
    var symbol              = OneSymbol()
    var grow                = Int()

    // in details::
    var tmin                = Int()
    var tmax                = Int()
    var sellSignalId        = Int()
    var content             = String()
    var conditions          = String()
    var lose                = Int()
    var goal                = Int()
    var powers              = Int()
    var viewCounter         = Int()
    var brokenAt            = String()
    var deletedAt           = NSDate()
    var updatedAt           = NSDate()

    // in progresses::
    var isFollowInProgress  = false
    var isReceived          = false
    
    init() {
        
    }
    init(json: JSON) {

        Arrow.setDateFormat("yyyy-MM-dd HH:mm:ss")
        
        id              <-- json["id"]
        symbolId        <-- json["symbol_id"]

        var riskStr     = String()
        riskStr         <-- json["risk"]
        if let risk     = SignalRisks.init(rawValue: riskStr) {
            self.risk   = risk
        }
        
        categoryId      <-- json["category_id"]
        title           <-- json["title"]
        summary         <-- json["summary"]
        price           <-- json["price"]
        limit           <-- json["limit"]
        sellCounter     <-- json["sell_counter"]
        isBroken        <-- json["is_broken"]
        createdAt       <-- json["created_at"]
        isAccessible    <-- json["is_accessible"]
        isFollowing     <-- json["is_following"]
        symbol          <== json["symbol"]
        grow            <-- json["grow"]
        
        tmin            <-- json["tmin"]
        tmax            <-- json["tmax"]
        sellSignalId    <-- json["sell_signal_id"]
        content         <-- json["content"]
        conditions      <-- json["conditions"]
        lose            <-- json["lose"]
        goal            <-- json["goal"]
        powers          <-- json["powers"]
        viewCounter     <-- json["view_counter"]
        brokenAt        <-- json["broken_at"]
        deletedAt       <-- json["deleted_at"]
        updatedAt       <-- json["updated_at"]
        
        
        
        
        if let json = (json as! NSDictionary).toString() {
            self.json = json
        }
        isReceived = DBSignals().exists(id)
        
        
    }
    
    enum SignalRisks: String {
        case high       = "high"
        case medium     = "medium"
        case low        = "low"
        
        func persianValue() -> String {
            switch self {
            case .high:
                return Strings.RISK_HIGH
            case .medium:
                return Strings.RISK_MEDIUM
            case .low:
                return Strings.RISK_LOW
            }
        }
    }

}