//
//  ResponseInit.swift
//  Signal
//
//  Created by Sina Khalili on 3/16/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Arrow

struct ResponseInit: ArrowParsable {
    
    var forceInit               = Bool()
    var summary                 = InitSummary()
    var user                    = InitUser()
    var categoriesUpdatedAt     = NSDate()
    var symbolsUpdatedAt        = NSDate()
    var useBanks                = Bool()
    var minVersion              = Int()
    var latestVersion           = Int()
    var offerUrl                = String()
    
    init() {
        
    }
    init(json: JSON) {
        
        Arrow.setDateFormat("yyyy-MM-dd HH:mm:ss")

        forceInit           <-- json["force_init"]
        summary             <== json["Summary"]
        user                <== json["User"]
        categoriesUpdatedAt <-- json["categories_updated_at"]
        symbolsUpdatedAt    <-- json["symbols_updated_at"]
        useBanks            <-- json["use_banks"]
        minVersion          <-- json["min_version"]
        latestVersion       <-- json["latest_version"]
        offerUrl            <-- json["offer_url"]
        
    }
    
}

/////

struct InitSummary: ArrowParsable {
    
    var unreadSignals           = Int()
    var unreadTutorials         = Int()
    var unreadAnalyses          = Int()
    var unreadNews              = Int()
    
    init() {
        
    }
    init(json: JSON) {
        unreadSignals   <-- json["unread_signals"]
        unreadTutorials <-- json["unread_tutorials"]
        unreadAnalyses  <-- json["unread_analyses"]
        unreadNews      <-- json["unread_news"]
    }
    
}

struct InitUser: ArrowParsable {
    
    var id                      = Int()
    var name                    = String()
    var email                   = String()
    var balance                 = Int()
    var subscriptionExpireAt  = NSDate()
    
    init() {
        
    }
    init(json: JSON) {
        
        Arrow.setDateFormat("yyyy-MM-dd HH:mm:ss")
        
        id                      <-- json["id"]
        name                    <-- json["name"]
        email                   <-- json["email"]
        balance                 <-- json["balance"]
        subscriptionExpireAt    <-- json["subscription_expire_at"]
    }

}