//
//  SignalButton.swift
//  Signal
//
//  Created by Sina Khalili on 2/18/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class SignalButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayer()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayer()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupLayer()
    }
    
    func setupLayer() {
        // Changing Font
        titleLabel?.font = UIFont(name: Constants.FONT, size: 15)
        // Background Color
        backgroundColor = UIColor(red: 17.0/255.0, green: 135.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        // Text Color
        setTitleColor(UIColor.whiteColor(), forState: .Normal)
        // Rounded Edge
        layer.cornerRadius = 5
        clipsToBounds = true
    }

}
