//
//  ViewPresenters.swift
//  Signal
//
//  Created by Sina Khalili on 3/5/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class ViewPresenters {

    static func openCategories(viewController: SignalViewController, categoryType: CategoryType, categoryId: Int?) {
        let categoriesNav = viewController.storyboard?.instantiateViewControllerWithIdentifier("categories") as! UINavigationController
        let categoriesVC  = categoriesNav.topViewController as! CategoriesViewController
        categoriesVC.categoryType = categoryType
        categoriesVC.categoryId   = categoryId
        categoriesVC.delegate     = viewController
        viewController.presentViewController(categoriesNav, animated: true, completion: nil)
    }

}