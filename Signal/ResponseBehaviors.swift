//
//  ResponseBahaviors.swift
//  Signal
//
//  Created by Sina Khalili on 2/29/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//


import Arrow

struct ResponseBehaviors: ArrowParsable {
    
    var data                = [OneBehavior]()
    var swing               = Int()
    var grow                = Int()

    init() {
        
    }
    init(json: JSON) {
        var dataArray = NSArray()
        dataArray       <-- json["Behaviors"]
        if(dataArray.count>0) {
            for i in 0...dataArray.count-1 {
                data.append(OneBehavior(json: dataArray[i]))
            }
        }
        swing           <-- json["swing"]
        grow            <-- json["grow"]
    }
    
}

/////

struct OneBehavior: ArrowParsable {
    
    var id                  = Int()
    var signalId            = Int()
    var description         = String()
    var createdAt           = NSDate()
    var updatedAt           = NSDate()
    
    init(json: JSON) {
        
        Arrow.setDateFormat("yyyy-MM-dd HH:mm:ss")
        
        id              <-- json["id"]
        signalId        <-- json["signal_id"]
        description     <-- json["description"]
        createdAt       <-- json["created_at"]
        updatedAt       <-- json["updated_at"]
        
    }
    
}
