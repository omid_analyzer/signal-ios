//
//  OneSignalCell.swift
//  Signal
//
//  Created by Sina Khalili on 2/20/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class OneSignalCell: UITableViewCell {

    @IBOutlet var imgSymbol: SignalImageView!

    @IBOutlet weak var lblTitle: SignalLabel!
    @IBOutlet var lblDate: SignalLabel!
    @IBOutlet weak var lblTime: SignalLabel!
    @IBOutlet weak var lblDescription: SignalLabel!
    @IBOutlet var viewBehaviors: UIView!
    @IBOutlet var btnBuy: SpecialButton!
    @IBOutlet var btnFollow: SpecialButton!
    @IBOutlet var indFollow: UIActivityIndicatorView!
    
    @IBOutlet var viewLimitHeight: NSLayoutConstraint!
    @IBOutlet var viewLimit: UIView!
    @IBOutlet var lblLimit: SignalLabel!
    
    @IBOutlet var lblGrow: SignalLabel!

}
