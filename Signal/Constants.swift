//
//  Constants.swift
//  Signal
//
//  Created by Sina Khalili on 2/18/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class Constants {
    
    static let FONT                         = "IRANSansMobile"
    static let FONT_MEDIUM                  = "IRANSansMobile-Medium"
    static let FONT_REGULAR                 = "IRANSansMobile-Regular"
    static let FONT_BOLD                    = "IRANSansMobile-Bold"

    static let PANTRY_LOGIN_INFO            = "loginInfo"
    static let PANTRY_LOGIN_INFO_IS_VALID   = "loginInfoIsValid"
    static let PANTRY_LAST_IDS              = "lastIds"
    static let PANTRY_CHACHED_INIT          = "cachedInit"
    static let PANTRY_CHACHED_TUTORIALS     = "cachedTutorials"
    static let PANTRY_CHACHED_ANALYSES      = "cachedAnalyses"
    static let PANTRY_CHACHED_SIGNALS       = "cachedSignals"
    
    static let Image_Lists_Header_TopBar    = "Lists_Header_TopBar"

    static let NoResponse                   = "NO_RESPONSE"
    static let NoResponseCode               = 0
    static let NoResponseJSON               = [ "ERRORS": NoResponse ]

}
