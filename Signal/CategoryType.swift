//
//  CategoryType.swift
//  Signal
//
//  Created by Sina Khalili on 3/4/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Foundation

enum CategoryType: String {
    case tutorial   = "tutorial"
    case analyse    = "analyse"
    case signal     = "signal"
    
    func persianValue() -> String {
        switch self {
        case .tutorial:
            return Strings.CATEGORY_TUTORIAL
        case .analyse:
            return Strings.CATEGORY_ANALYSE
        case .signal:
            return Strings.CATEGORY_SIGNAL
        }
    }
}