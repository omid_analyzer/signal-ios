//
//  LoginViewController.swift
//  Signal
//
//  Created by Sina Khalili on 2/19/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import SwiftValidator
import Pantry

class LoginViewController: SignalViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let ROW_EMAIL = 0, ROW_PASSWORD = 1
    
    var txtEmail        = UITextField()
    var txtPassword     = UITextField()
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewDidLoad() {
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        addTitleLogo()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("inputTextCell", forIndexPath: indexPath) as! InputTextTableViewCell
        
        cell.layoutMargins = UIEdgeInsetsZero
        // next on keyboard return
        cell.textField.tag = indexPath.row+1
        
        switch indexPath.row {
            
        case ROW_EMAIL:
            cell.label.text = "پست الکترونیک"
            cell.textField.placeholder = "email@domain.com"
            cell.textField.keyboardType = .EmailAddress
            cell.textField.becomeFirstResponder()
            cell.textField.text = txtEmail.text
            txtEmail = cell.textField
            
        case ROW_PASSWORD:
            cell.label.text = "رمز عبور"
            cell.textField.placeholder = "******"
            cell.textField
                .secureTextEntry = true
            cell.textField.returnKeyType = .Go
            txtPassword = cell.textField
            
        default:
            break
        }
        
        return cell
    }
    
    @IBAction func loginPressed(sender: AnyObject) {
        login()
    }
    override func submit() {
        login()
    }
    func login() {
        
        WebServices.login(txtEmail, txtPassword: txtPassword, view: tableView, success: { (response) -> () in
            if let urlResponse = response {
                let loginInfo = LoginInfo(username: self.txtEmail.text!, json: urlResponse)
                Pantry.pack(loginInfo, key: Constants.PANTRY_LOGIN_INFO)
                Pantry.pack(true, key: Constants.PANTRY_LOGIN_INFO_IS_VALID, expires: StorageExpiry.Seconds(Double(loginInfo.expiresIn)))
                
                (UIApplication.sharedApplication().delegate as! AppDelegate).loginInfo = loginInfo
                
                GeneralFunctions.hideProgress()
                self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
            }
            }, failure: { (code, response) -> () in
                // error
                self.hideProgress()
                if(response==nil || !ResponseErrorProcessor.process(response!, view: self.tableView)) {
                    self.tableView.messageError("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                }
            })
    
    }
    
}
