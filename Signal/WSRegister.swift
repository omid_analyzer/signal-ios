//
//  WSRegister.swift
//  Signal
//
//  Created by Sina Khalili on 3/3/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftValidator
import Pantry

extension WebServices {
    
    static func register(txtName: UITextField, txtEmail: UITextField, txtPhone: UITextField, txtPassword: UITextField, txtPresenter: UITextField, view: UIView, success: Success? = nil, failure: Failure? = nil, startFailure: StartFailure? = nil) {
        
        // validating inputs
        
        let validator = Validator()
        validator.registerField(txtName, rules: [RequiredRule(message: Strings.ENTER_NAME)])
        validator.registerField(txtEmail, rules: [RequiredRule(message: Strings.ENTER_EMAIL), EmailRule(message: Strings.ERROR_EMAIL_INVALID)])
        validator.registerField(txtPassword, rules: [RequiredRule(message: Strings.ENTER_PASSWORD), MinLengthRule(length: 6, message: Strings.ERROR_PASSWORD_SHORT)])
        validator.validate { error in
            var message = String()
            for (_, error) in validator.errors {
                message = "\(message)\n\(error.errorMessage)"
            }
            if(message.characters.count>1) {
                view.messageWarning(message.substringFromIndex(message.startIndex.advancedBy(1)))
            }
        }
        if validator.errors.count>0 {
            startFailure?(error: AppInternalError(code: .ValidationError))
            return
        }
        
        // checking connection
        if(!Reachability.isConnectedToNetwork()) {
            view.messageWarning(Strings.NOT_CONNECTED_TO_INTERNET)
            startFailure?(error: AppInternalError(code: .NotConnectedToInternet))
            return
        }
        
        // calling web service
        
        let name        = txtName.text!
        let email       = txtEmail.text!
        let phone       = txtPhone.text!
        let password    = txtPassword.text!
        let presenter   = txtPresenter.text!
        
        GeneralFunctions.showProgress()
        
        let params = ["name": name
            , "email": email
            , "mobile": phone
            , "password": password
            , "presenter_email": presenter]
        
        Alamofire.request(.POST, URLs.REGISTER, parameters: params)
            .responseJSON { response in
                if let res = response.response {
                    if res.statusCode == 200 {
                        success?(response: response.result.value)
                    } else {
                        failure?(code: res.statusCode, response: response.result.value)
                    }
                } else {
                    failure?(code: Constants.NoResponseCode, response: Constants.NoResponseJSON)
                }
        }

    }
    
}