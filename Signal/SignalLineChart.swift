//
//  SignalLineChart.swift
//  Signal
//
//  Created by Sina Khalili on 2/29/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class SignalLineChart: LineChart {

    override init(frame: CGRect) {
        super.init(frame: frame)
        animation.enabled = true
        area = false
        x.labels.visible = true
        y.labels.visible = true
        let color = Colors.lightBlue
        y.grid.color = color
        y.axis.visible = false
        x.grid.color = UIColor.clearColor()
        colors = [
            UIColor.whiteColor()
            , UIColor(colorLiteralRed: 112.0/255.0, green: 1, blue: 69.0/255.0, alpha: 1)
        ]
    }

    required internal init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
