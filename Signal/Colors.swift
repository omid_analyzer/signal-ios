//
//  Colors.swift
//  Signal
//
//  Created by Sina Khalili on 2/29/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class Colors {
    static let lightBlue    = UIColor(red: 112/255.0, green: 182.0/255.0, blue: 212.0/255.0, alpha: 1)
    static let blue         = UIColor(red: 51.0/255.0, green: 169.0/255.0, blue: 220.0/255.0, alpha: 1)
    static let darkBlue     = UIColor(red: 0, green: 122.0/255.0, blue: 1, alpha: 1)
    static let specialBlue  = UIColor(red: 0, green: 156.0/255.0, blue: 246.0/255.0, alpha: 1)
    static let green        = UIColor(red: 52.0/255.0, green: 192.0/255.0, blue: 82.0/255.0, alpha: 1)

}