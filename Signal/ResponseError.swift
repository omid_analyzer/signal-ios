//
//  ResponseError.swift
//  Signal
//
//  Created by Sina Khalili on 2/19/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Arrow

struct ResponseError:ArrowParsable {
    
    var errors = NSArray()

    init() {
        
    }
    init(json: JSON) {
        errors <-- json["Errors"]
    }
}