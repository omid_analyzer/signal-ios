//
//  RegisterViewController.swift
//  Signal
//
//  Created by Sina Khalili on 2/18/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import SwiftValidator
import Pantry

class RegisterViewController: SignalViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    let ROW_FULL_NAME = 0, ROW_EMAIL = 1, ROW_PHONE = 2, ROW_PASSWORD = 3, ROW_PRESENTER = 4
    
    var txtName         = UITextField()
    var txtEmail        = UITextField()
    var txtPhone        = UITextField()
    var txtPassword     = UITextField()
    var txtPresenter    = UITextField()
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewDidLoad() {
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        addTitleLogo()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("inputTextCell", forIndexPath: indexPath) as! InputTextTableViewCell
        
        cell.layoutMargins = UIEdgeInsetsZero
        // next on keyboard return
        cell.textField.tag = indexPath.row+1
        
        switch indexPath.row {
            
        case ROW_FULL_NAME:
            cell.label.text = "نام کامل"
            cell.textField.placeholder = "نام و نام خانوادگی"
            cell.textField.becomeFirstResponder()
            txtName = cell.textField
            break
            
        case ROW_EMAIL:
            cell.label.text = "پست الکترونیک"
            cell.textField.placeholder = "email@domain.com"
            cell.textField.keyboardType = .EmailAddress
            cell.textField.text = txtEmail.text
            txtEmail = cell.textField
            
        case ROW_PHONE:
            cell.label.text = "شماره همراه"
            cell.textField.placeholder = "09122345678"
            txtPhone = cell.textField
            
        case ROW_PASSWORD:
            cell.label.text = "رمز عبور"
            cell.textField.placeholder = "حدأقل ۶ حرف"
            cell.textField
                .secureTextEntry = true
            txtPassword = cell.textField
            
        case ROW_PRESENTER:
            cell.label.text = "معرف"
            cell.textField.placeholder = "email@domain.com (اختیاری)"
            cell.textField.keyboardType = .EmailAddress
            cell.textField.returnKeyType = .Go
            txtPresenter = cell.textField
            break
            
        default:
            break
        }
        
        return cell
    }
    
    @IBAction func registerPressed(sender: AnyObject) {
        register()
    }
    override func submit() {
        register()
    }
    func register() {
        
        WebServices.register(txtName, txtEmail: txtEmail, txtPhone: txtPhone, txtPassword: txtPassword, txtPresenter: txtPresenter, view: tableView, success:  { (response) -> () in
                // response
                self.hideProgress()
                self.tableView.messageSuccess("ثبت نام با موفقیت انجام شد.")
            }, failure: { (code, response) -> () in
                // error
                self.hideProgress()
                if(response==nil || !ResponseErrorProcessor.process(response!, view: self.tableView)) {
                    self.tableView.messageError("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                }
        })
        
    }
    
}
