//
//  ResponseSignalChart.swift
//  Signal
//
//  Created by Sina Khalili on 3/16/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Foundation
import Arrow

struct ResponseSignalChart: ArrowParsable {
    
    var timeSlices                  = [String]()
    var signalsEquity               = [CGFloat]()
    var marketEquity                = [CGFloat]()
    var continuousSuccessSignals    = Int()
    var successSignals              = Int()
    var failedSignals               = Int()
    var successToFailRatio          = Int()
    
    init() {
        
    }
    init(json: JSON) {
        timeSlices                  <-- json["TimeSlices"]
        signalsEquity               <-- json["SignalsEquity"]
        marketEquity                <-- json["MarketEquity"]
        continuousSuccessSignals    <-- json["continuous_success_signals"]
        successSignals              <-- json["success_signals"]
        failedSignals               <-- json["failed_signals"]
        successToFailRatio          <-- json["success_to_fail_ratio"]
    }
    
}
