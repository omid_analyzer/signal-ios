//
//  ResponseCategories.swift
//  Signal
//
//  Created by Sina Khalili on 3/4/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Arrow

struct ResponseCategories: ArrowParsable {
    
    var data                = [OneCategory]()
    
    init() {
        
    }
    init(json: JSON) {
        var dataArray = NSArray()
        dataArray       <-- json["Categories"]
        if(dataArray.count>0) {
            for i in 0...dataArray.count-1 {
                data.append(OneCategory(json: dataArray[i]))
            }
        }
    }
    
}

/////

struct OneCategory: ArrowParsable {
    
    var id                  = Int()
    var title               = String()
    var description         = String()
    
    init() {
        
    }
    init(json: JSON) {
        id              <-- json["id"]
        title           <-- json["title"]
        description     <-- json["description"]
    }
    
}