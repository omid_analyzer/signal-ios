//
//  SignalContainerView.swift
//  Signal
//
//  Created by Sina Khalili on 2/29/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class SignalContainerView: UIView {

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayer()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupLayer()
    }
    
    func setupLayer(cornerRadius: CGFloat = 5, shadowRadius: CGFloat = 0) {

        // rounded corner
        self.layer.cornerRadius = cornerRadius
        
        // shadow
        layer.shadowColor = UIColor.blackColor().CGColor;
        layer.shadowOffset = CGSizeZero;
        layer.shadowRadius = shadowRadius;
        layer.shadowOpacity = 0.5;
        layer.masksToBounds = false;

        // clips to bounds
        clipsToBounds = false
    
    }

}
