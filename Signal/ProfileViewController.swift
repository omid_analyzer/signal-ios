//
//  ProfileViewController.swift
//  Signal
//
//  Created by Sina Khalili on 2/18/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import Alamofire
import Pantry

class ProfileViewController: SignalViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var imgUser: SignalImageView!
    @IBOutlet var lblUsername: SignalLabel!
    
    @IBOutlet var topBarBackground: UIImageView!
    @IBOutlet var listHeaderBackground: UIImageView!
    @IBOutlet var listHeaderBackgroundY: NSLayoutConstraint!
    @IBOutlet var listHeaderBackgroundHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    let ROW_DETAILS = 0, ROW_NEWS = 1, ROW_LOGOUT = 2
    
    override func viewDidLoad() {

        initWhiteNavigationBar()
        initRefreshControl(tableView, color: UIColor.whiteColor())
        self.tableView.separatorStyle       = .None
        self.tableView.rowHeight            = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight   = 240
        scrollViewDidScroll(tableView)
        
        self.navigationItem.title = " "
        
        imgUser.makeRoundedCorners(42.5)
        
        updateItems()
        
        downloadData()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBar.hidden = true
    }
    
    override func refresh(sender: AnyObject) {
        isRefreshing = true
        Alamofire.Manager.cancelRequests(URLs.INITIALIZE)
        downloadData()
    }
    
    override func endRefreshing() {
        super.endRefreshing()
    }
    
    func updateItems() {
        let appDelegate     = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let responseInit    = appDelegate.responseInit

        imgUser.load(responseInit.user.email.gravatar(), placeholder: UIImage(named: "Logo"), completionHandler: nil)

        lblUsername.text        = responseInit.user.name
        
        tableView.reloadData()
    }
    
    func downloadData() {
        
        WebServices.initialize(self, view: self.view, success: { (response) -> () in
            
            // set global login info
            let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            appDelegate.responseInit    = ResponseInit(json: response!)
            
            self.updateItems()
            
        })
        
    }
    
    func scrollToTop(sender: AnyObject) {
        self.tableView.setContentOffset(CGPoint.zero, animated:true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let responseInit = (UIApplication.sharedApplication().delegate as! AppDelegate).responseInit
        
        switch indexPath.row {
        case ROW_DETAILS:
            let cell = tableView.dequeueReusableCellWithIdentifier("detailsCell", forIndexPath: indexPath) as! ProfileDetailsCell
            cell.lblBalance.text        = responseInit.user.balance.formattedPrice()
            var remainingDays = NSDate().numberOfDaysUntilDateTime(responseInit.user.subscriptionExpireAt)
            if remainingDays < 0 {
                remainingDays = 0
            }
            cell.lblDaysToExpire.text   = remainingDays.persianDigits()
            cell.viewContainer.setupLayer(0, shadowRadius: 0)
            return cell
        case ROW_NEWS:
            let cell = tableView.dequeueReusableCellWithIdentifier("newsCell", forIndexPath: indexPath) as! ProfileNewsCell
            if responseInit.summary.unreadNews > 0 {
                cell.lblBadge.text          = responseInit.summary.unreadNews.persianDigits()
                cell.viewBadge.setupLayer(10)
                cell.viewBadge.hidden       = false
            } else {
                cell.viewBadge.hidden       = true
            }
            return cell
        case ROW_LOGOUT:
            let cell = tableView.dequeueReusableCellWithIdentifier("logoutCell", forIndexPath: indexPath)
            let btnLogout = cell.viewWithTag(1) as! UIButton
            let gesLogout = UITapGestureRecognizer(target: self, action: "logout:")
            btnLogout.addGestureRecognizer(gesLogout)
            return cell
        default:
            break
        }
        
        return UITableViewCell()
        
    }
    
    func logout(sender: AnyObject!) {
        if let _: AppLastIds = Pantry.unpack(Constants.PANTRY_LAST_IDS) {
            Pantry.expire(Constants.PANTRY_LAST_IDS)
        }
        Pantry.expire(Constants.PANTRY_LOGIN_INFO)
        Pantry.expire(Constants.PANTRY_LOGIN_INFO_IS_VALID)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // topBar
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        if yOffset <= 0 {
            listHeaderBackgroundY.constant          = -20
            listHeaderBackgroundHeight.constant     = 210 - yOffset
            topBarBackground.image                  = nil
        } else {
            listHeaderBackgroundY.constant          = -20 - yOffset
            listHeaderBackgroundHeight.constant     = 210
            topBarBackground.image                  = UIImage(named: Constants.Image_Lists_Header_TopBar)
        }
    }
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
}
