//
//  ResponseErrorProcessor.swift
//  Signal
//
//  Created by Sina Khalili on 2/19/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import SwiftValidator

class ResponseErrorProcessor {
    
    static func process(urlResponse: AnyObject, view: UIView) -> Bool {

        let responseError = ResponseError(json: urlResponse)

        if responseError.errors.count>0 {
            var warningMessage = String()
            var errorMessage = String()

            switch responseError.errors[0] as! String {
            
            case "EMAIL_TAKEN":
                warningMessage = Strings.ERROR_EMAIL_TAKEN
                break
                
            case "EMAIL_INVALID":
                warningMessage = Strings.ERROR_EMAIL_INVALID
                break
                
            case "NAME_INVALID":
                warningMessage = Strings.ERROR_NAME_INVALID
                break
            
            case "MOBILE_INVALID":
                warningMessage = Strings.ERROR_MOBILE_INVALID
                break

            case "PASSWORD_INVALID":
                warningMessage = Strings.ERROR_PASSWORD_INVALID
                break

            case "PRESENTER_EMAIL_INVALID":
                warningMessage = Strings.ERROR_PRESENTER_EMAIL_INVALID
                break
                
            case "PRESENTER_EMAIL_NOT_FOUND":
                warningMessage = Strings.ERROR_PRESENTER_EMAIL_NOT_FOUND
                break

            case "AUTH_EXCEPTION":
                errorMessage   = Strings.ERROR_AUTH_EXCEPTION
                break
                
            case "USER_NOT_FOUND":
                errorMessage   = Strings.ERROR_USER_NOT_FOUND
                break
                
            case "UNIQUE_ID_INVALID":
                errorMessage   = Strings.ERROR_UNIQUE_ID_INVALID
                break
                
            case "UNKNOWN_ERROR":
                errorMessage   = Strings.ERROR_UNKNOWN
                break
                
            case "BALANCE_NOT_ENOUGH":
                errorMessage   = Strings.ERROR_BALANCE_NOT_ENOUGH
                break
                
            case "PAYMENT_VERIFIED_BEFORE":
                warningMessage = Strings.ERROR_PAYMENT_VERIFIED_BEFORE
                break
                
            case "PAYMENT_ERROR":
                errorMessage   = Strings.ERROR_PAYMENT
                break
            
            case "PLATFORM_LIMIT":
                errorMessage   = Strings.ERROR_PLATFORM_LIMIT
                break
                
            case "INTERNAL_ERROR":
                errorMessage   = Strings.ERROR_INTERNAL
                break
                
            case "NOT_PAID_BEFORE":
                errorMessage   = Strings.ERROR_NOT_PAID_BEFORE
                break
                
            case "DUPLICATE_REQUEST":
                errorMessage   = Strings.ERROR_DUPLICATE_REQUEST
                break
            
            case "SIGNAL_OUT_OF_SELL":
                errorMessage   = Strings.ERROR_SIGNAL_OUT_OF_SELL
                break
                
            case "SIGNAL_ID_INVALID":
                errorMessage   = Strings.ERROR_SIGNAL_ID_INVALID
                break
            case "SIGNAL_ID_NOT_FOUND":
                errorMessage   = Strings.ERROR_SIGNAL_ID_INVALID
                break
            
            case "ANALYSE_ID_INVALID":
                errorMessage   = Strings.ERROR_ANALYSE_ID_INVALID
                break
            case "ANALYSE_ID_NOT_FOUND":
                errorMessage   = Strings.ERROR_ANALYSE_ID_INVALID
                break
                
            case "TUTORIAL_ID_INVALID":
                errorMessage   = Strings.ERROR_TUTORIAL_ID_INVALID
                break
            case "TUTORIAL_ID_NOT_FOUND":
                errorMessage   = Strings.ERROR_TUTORIAL_ID_INVALID
                break
                
            case "FROM_DATE_INVALID":
                errorMessage   = Strings.ERROR_IN_PARAMETERS
                break
            case "TO_DATE_INVALID":
                errorMessage   = Strings.ERROR_IN_PARAMETERS
                break
            case "LAST_SIGNAL_ID_INVALID":
                errorMessage   = Strings.ERROR_IN_PARAMETERS
                break
            case "LAST_ANALYSE_ID_INVALID":
                errorMessage   = Strings.ERROR_IN_PARAMETERS
                break
            case "LAST_TUTORIAL_ID_INVALID":
                errorMessage   = Strings.ERROR_IN_PARAMETERS
                break
            case "LAST_NEWS_ID_INVALID":
                errorMessage   = Strings.ERROR_IN_PARAMETERS
                break
            case "IS_LAZY_LOAD_INVALID":
                errorMessage   = Strings.ERROR_IN_PARAMETERS
                break
            case "FROM_ID_INVALID":
                errorMessage   = Strings.ERROR_IN_PARAMETERS
                break
                
            case "KEY_INVALID":
                errorMessage   = Strings.ERROR_KEY_INVALID
                break
                
            case "SUBSCRIPTION_PLANE_ID_INVALID":
                warningMessage = Strings.ERROR_SUBSCRIPTION_PLANE_ID_INVALID
                break
            case "SUBSCRIPTION_PLANE_ID_NOT_FOUND":
                warningMessage = Strings.ERROR_SUBSCRIPTION_PLANE_ID_INVALID
                break
                
            case "PRODUCT_ID_INVALID":
                errorMessage   = Strings.ERROR_PRODUCT_ID_INVALID
                break
            case "PRODUCT_ID_NOT_FOUND":
                warningMessage = Strings.ERROR_PRODUCT_NOT_FOUND
                break
                
            case "PAYMENT_TOKEN_INVALID":
                errorMessage   = Strings.ERROR_PAYMENT_TOKEN_INVALID
                break
            case "PAYMENT_TOKEN_NOT_FOUND":
                errorMessage   = Strings.ERROR_PAYMENT_TOKEN_INVALID
                break
                
            case "AMOUNT_INVALID":
                warningMessage = Strings.ERROR_AMOUNT_INVALID
                break
            
            case "DESCRIPTION_INVALID":
                warningMessage = Strings.ERROR_DESCRIPTION_INVALID
                break
            
            case "AUTHORITY_INVALID":
                errorMessage   = Strings.ERROR_AUTHORITY_INVALID
                break
            case "AUTHORITY_NOT_FOUND":
                errorMessage   = Strings.ERROR_AUTHORITY_INVALID
                break
            
            case "STATUS_INVALID":
                errorMessage   = Strings.ERROR_STATUS_INVALID
                break

            case "ACCESS_DENIED":
                RefreshToken.refresh()                                      // refresh token!
                break
                
            case Constants.NoResponse:
                errorMessage   = Strings.ERROR_NO_RESPONSE
                break

            default:
                warningMessage = responseError.errors[0] as! String
                break
            }
            
            if errorMessage.characters.count>0 {
                view.messageError(errorMessage)
            } else {
                view.messageWarning(warningMessage)
            }
            
            return true        // error occured and the error message shown
        } else {
            return false       // no errors!
        }
    }
    
}