//
//  GeneralFunctions.swift
//  Signal
//
//  Created by Sina Khalili on 2/20/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import PKHUD
import Pantry
import CryptoSwift
import TSMessages
import Arrow

class GeneralFunctions {
    
    static func showProgress() {
        hideProgress()
        progressInFront(true)
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    static func hideProgress() {
        PKHUD.sharedHUD.hide(animated: true, completion: nil)
    }
    
    static func isTokenValid() -> Bool {
        if let isValid: Bool = Pantry.unpack(Constants.PANTRY_LOGIN_INFO_IS_VALID) {
            if isValid {
                return true
            } else {
                if (UIApplication.sharedApplication().delegate as! AppDelegate).loginInfo.accessToken.characters.count > 0 {
                    RefreshToken.refresh()
                }
                return false
            }
        } else {
            return false
        }
    }
    
    static func showText(string: String, delay: Double? = nil) {
        hideProgress()
        progressInFront(false)
        if delay != nil {
            HUD.flash(HUDContentType.Label(string), delay: delay!)
        } else {
            HUD.show(HUDContentType.Label(string))
        }
        (PKHUD.sharedHUD.contentView as! PKHUDTextView).titleLabel.font = UIFont(name: Constants.FONT, size: 17)
    }
    
    private static func progressInFront(inFront: Bool) {
        PKHUD.sharedHUD.userInteractionOnUnderlyingViewsEnabled = !inFront
        PKHUD.sharedHUD.dimsBackground                          = inFront
    }
    
}

extension Int {
    
    func formattedNumber() -> String {
        let number = NSNumber(integer: self)
        let formatter = NSNumberFormatter()
        let locale = NSLocale(localeIdentifier: "fa")
        formatter.locale = locale
        formatter.numberStyle = .DecimalStyle
        return formatter.stringFromNumber(number)!
    }
    
    func formattedPrice() -> String {
        return self > 0 ? "\(formattedNumber()) تومان" : Strings.FREE
    }
    
    /**
     Converts the int digits to persian characters
     
     - returns: String of number with persian characters
     */
    func persianDigits() -> String {
        let number = NSNumber(integer: self)
        let formatter = NSNumberFormatter()
        let locale = NSLocale(localeIdentifier: "fa")
        formatter.locale = locale
        formatter.numberStyle = .NoStyle
        return formatter.stringFromNumber(number)!
    }
    
    /**
     Converts the integer to h,m,s format to show (+persian digits)
     
     - returns: String in h,m,s format to show
     */
    func timeDigits() -> String {
        if self >= 10 {
            return persianDigits()
        } else {
            return "۰\(persianDigits())"
        }
    }
    
}

extension String {
    
    subscript (i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = startIndex.advancedBy(r.startIndex)
        let end = start.advancedBy(r.endIndex - r.startIndex)
        return self[Range(start: start, end: end)]
    }
    
    func gravatar() -> String {
        return "http://www.gravatar.com/avatar/\(self.md5())"
    }
    
    func toDateFrom_yyyy_MM_dd() -> NSDate? {
        let formatter  = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.dateFromString(self)
    }
    
    func toDictionaryAsAnyObject() -> AnyObject? {
        if let data = dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: [])
            } catch {
                return nil
            }
        } else {
            return nil
        }
    }
    
}

extension NSDictionary {
    func toString() -> String? {
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(self as AnyObject, options: NSJSONWritingOptions.PrettyPrinted)
            return NSString(data: jsonData, encoding: NSUTF8StringEncoding) as? String
        } catch _ as NSError {
            return nil
        }
    }
}

extension NSDate {
    struct Calendar {
        static let gregorian = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
    }
    func isInSameDayAs(date date: NSDate) -> Bool {
        return Calendar.gregorian.isDate(self, inSameDayAsDate: date)
    }
    func isBeforeDay(date date: NSDate) -> Bool {
        return date.timeIntervalSince1970-24*60*60 > self.timeIntervalSince1970
    }
    
    /**
     Converts the NSDate to Persian Date Format
     
     - returns: Persian date
     */
    func persianDate() -> String {
        let calendar    = NSCalendar.currentCalendar()
        let components  = calendar.components([.Day , .Month , .Year], fromDate: self)
        let jalaali     = JDF().toJalaali(components.year, gm: components.month, gd: components.day)
        return "\(jalaali.day.persianDigits()) \(JDF.monthName[jalaali.month-1]) \(jalaali.year.persianDigits())"
    }
    
    /**
     Converts the NSDate's time to Persian Time Format
     
     - returns: Persian time (hour:minute)
     */
    func persianTime() -> String {
        let calendar    = NSCalendar.currentCalendar()
        let components  = calendar.components([.Hour, .Minute], fromDate: self)
        return "\(components.hour.timeDigits()):\(components.minute.timeDigits())"
        
    }
    
    /**
     If it's today returns the time ago
     Else returns persian date
     
     - returns: Date to show
     */
    func smartDate() -> String {
        if isInSameDayAs(date: NSDate()) {
            return timeAgo
        } else {
            return persianDate()
        }
    }
    
    /**
     If it's today returns nothing
     Else returns persian time
     
     Notice: If it's today, we have shown time in smartDate
     
     - returns: Time to show
     */
    func smartTime() -> String {
        if isInSameDayAs(date: NSDate()) {
            return ""
        } else {
            return persianTime()
        }
    }
    
    /**
     Returns something like : دوشنبه ۱ اسفند ۱۳۹۴ . ۲۳:۲۳
     
     - returns: Persian Date Time
     */
    func persianDateTime() -> String {
        return "\(getDayOfWeekInPersian()) \(persianDate()) . \(persianTime())"
    }
    
    /**
     Days between two NSDate
     
     - parameter toDateTime: to NSDate...
     - parameter timeZone:   optional TimeZone
     
     - returns: Days number
     */
    func numberOfDaysUntilDateTime(toDateTime: NSDate, inTimeZone timeZone: NSTimeZone? = nil) -> Int {
        let calendar = NSCalendar.currentCalendar()
        if let timeZone = timeZone {
            calendar.timeZone = timeZone
        }
        
        var fromDate: NSDate?, toDate: NSDate?
        
        calendar.rangeOfUnit(.Day, startDate: &fromDate, interval: nil, forDate: self)
        calendar.rangeOfUnit(.Day, startDate: &toDate, interval: nil, forDate: toDateTime)
        
        let difference = calendar.components(.Day, fromDate: fromDate!, toDate: toDate!, options: [])
        return difference.day
    }
    
    func getDayOfWeek()->Int {
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = myCalendar.components(.Weekday, fromDate: self)
        let weekDay = myComponents.weekday
        return weekDay
    }
    func getDayOfWeekInPersian()->String {
        let days = ["یکشنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنجشنبه", "جمعه", "شنبه"]
        let dayOfWeek = getDayOfWeek()
        if dayOfWeek < 8 {
            return days[dayOfWeek-1]
        } else {
            return ""
        }
    }
    
}

extension UIView {
    
    func messageSuccess(message: String) {
        TSMessage.showNotificationInViewController(self.viewController(), title: message, subtitle: nil, type: .Success)
    }
    func messageWarning(message: String) {
        TSMessage.showNotificationInViewController(self.viewController(), title: message, subtitle: nil, type: .Warning)
    }
    func messageError(message: String) {
        TSMessage.showNotificationInViewController(self.viewController(), title: message, subtitle: nil, type: .Error)
    }
    
}