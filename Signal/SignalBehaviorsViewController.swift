//
//  SignalBehaviorsViewController.swift
//  Signal
//
//  Created by Sina Khalili on 2/29/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import Alamofire

class SignalBehaviorsViewController: SignalViewController {
    
    @IBOutlet var headerBackgroundHeight: NSLayoutConstraint!
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    @IBOutlet var lblSwing: SignalLabel!
    @IBOutlet var lblGrow: SignalLabel!
    
    var signal          = OneSignal()
    var lastResponse    = ResponseBehaviors()
    var items           = [OneBehavior]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initWhiteNavigationBar()
        
        title = signal.title
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight    = 80
        
        initRefreshControl(tableView, color: nil)
        indicator.startAnimating()
        downloadData()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        hideProgress()
    }
    
    override func refresh(sender: AnyObject) {
        hideProgress()
        Alamofire.Manager.cancelRequests(URLs.GET_SIGNALS)
        downloadData()
    }
    
    override func endRefreshing() {
        super.endRefreshing()
        Alamofire.Manager.cancelRequests(URLs.GET_SIGNALS)
        self.indicator.hidden = true
        self.indicator.stopAnimating()
    }
    
    func updateItems() {
        lblSwing.text   = lastResponse.swing.persianDigits()
        lblGrow.text    = lastResponse.grow.persianDigits()
        tableView.reloadData()
    }
    
    func downloadData() {
        
        
        WebServices.getSignalBehaviors(signal.id, viewController: self, view: tableView, success: { (response) -> () in
            
            // response
            self.endRefreshing()
            self.endLoadingMore()
            self.refreshControl.endRefreshing()
            if response != nil {
                self.lastResponse = ResponseBehaviors(json: response!)
                self.items = self.lastResponse.data
                self.updateItems()
                if self.items.count == 0 {
                    GeneralFunctions.showText(Strings.NO_SIGNAL_BEHAVIORS)
                }
            }
            
            }, failure: { (code, response) in
                
                // error
                self.endRefreshing()
                self.endLoadingMore()
                if(response==nil || !ResponseErrorProcessor.process(response!, view: self.tableView)) {
                    self.tableView.messageError("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                }
        })
    }
    
    func scrollToTop(sender: AnyObject) {
        self.tableView.setContentOffset(CGPoint.zero, animated:true)
    }
    
    // table view implementation
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("oneBehaviorCell", forIndexPath: indexPath) as! OneBehaviorCell
        
        // showing signal
        let oneItem                 = items[indexPath.row]
        cell.desc.text              = oneItem.description
        cell.time.text              = oneItem.createdAt.persianDateTime()
        
        return cell
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        headerBackgroundHeight.constant     = 0 - yOffset
    }
    
}
