//
//  AppInternalError.swift
//  Signal
//
//  Created by Sina Khalili on 3/3/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Foundation

struct AppInternalError {
    
    var code: AppInternalErrorCode
    
    init(code: AppInternalErrorCode) {
        self.code = code
    }
}

enum AppInternalErrorCode: Int {
    case NotConnectedToInternet = -1
    case ValidationError        = -2
    case InvalidToken           = -3
}