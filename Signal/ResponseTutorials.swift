//
//  ResponseTutorials.swift
//  Signal
//
//  Created by Sina Khalili on 2/21/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import Arrow

class ResponseTutorials: ArrowParsable {
    
    var total               = Int()
    var perPage             = Int()
    var currentPage         = Int()
    var lastPage            = Int()
    var nextPageUrl         = String()
    var prevPageUrl         = String()
    var from                = Int()
    var to                  = Int()
    var data                = [OneTutorial]()
    
    init() {
        
    }
    required init(json: JSON) {
        total           <-- json["total"]
        perPage         <-- json["per_page"]
        currentPage     <-- json["current_page"]
        lastPage        <-- json["last_page"]
        nextPageUrl     <-- json["next_page_url"]
        prevPageUrl     <-- json["prev_page_url"]
        from            <-- json["from"]
        to              <-- json["to"]
        var dataArray = NSArray()
        dataArray       <-- json["data"]
        if(dataArray.count>0) {
            for i in 0...dataArray.count-1 {
                data.append(OneTutorial(json: dataArray[i]))
            }
        }
    }
    
}

/////

struct OneTutorial: ArrowParsable {

    var id                  = Int()
    var categoryId          = Int()
    var userId              = Int()
    var title               = String()
    var summary             = String()
    var price               = Int()
    var createdAt           = NSDate()
    var isAccessible        = Bool()
    var category            = OneCategory()

    init(json: JSON) {

        Arrow.setDateFormat("yyyy-MM-dd HH:mm:ss")

        id              <-- json["id"]
        categoryId      <-- json["category_id"]
        userId          <-- json["user_id"]
        title           <-- json["title"]
        summary         <-- json["summary"]
        price           <-- json["price"]
        createdAt       <-- json["created_at"]
        category        <== json["category"]
        isAccessible    <-- json["isAccessible"]
    }
    
}