//
//  SignalSegmentedControl.swift
//  Signal
//
//  Created by Sina Khalili on 2/20/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class SignalSegmentedControl: UISegmentedControl {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayer()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupLayer()
    }
    
    func setupLayer() {
        // Changing Font
        let attributes = [NSFontAttributeName:UIFont(name: Constants.FONT_MEDIUM, size: 12)!]
        setTitleTextAttributes(attributes, forState: .Normal)
    }
    
    

}