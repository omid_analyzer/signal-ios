//
//  DetailCell.swift
//  Signal
//
//  Created by Sina Khalili on 3/17/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {

    @IBOutlet var lblTitle: SignalLabel!
    @IBOutlet var lblDetail: SignalLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
