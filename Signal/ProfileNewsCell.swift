//
//  ProfileNewsCell.swift
//  Signal
//
//  Created by Sina Khalili on 3/16/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class ProfileNewsCell: UITableViewCell {
    
    @IBOutlet var lblBadge: UILabel!
    @IBOutlet var viewBadge: SignalContainerView!
    
}