//
//  RefreshToken.swift
//  Signal
//
//  Created by Sina Khalili on 2/20/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import Alamofire
import Pantry
import PKHUD

class RefreshToken {
    
    static func refresh(showProgress: Bool? = true) {
        
        // checking connection
        if(!Reachability.isConnectedToNetwork()) {
            // show error in active view
            if let viewController = UIApplication.sharedApplication().keyWindow?.rootViewController {
                if viewController.respondsToSelector(Selector("showWarning:")) {
                    viewController.performSelector(Selector("showWarning:"), withObject: Strings.NOT_CONNECTED_TO_INTERNET)
                }
            }
            return
        }

        // show progress
        if showProgress! == true {
            GeneralFunctions.showProgress()
        }

        // calling webservice
        let oldLoginInfo: LoginInfo = Pantry.unpack(Constants.PANTRY_LOGIN_INFO)!
        let params = ["grant_type": "refresh_token"
            , "client_id":          "QcoPTcFz4uaXoAQLpProsCEKfY1q2Z0I1clKfR79"
            , "client_secret":      "fsz7XAHIxn3G26K04CFylHV09SIkTreWJCUv0p3l"
            , "refresh_token":       oldLoginInfo.accessToken]
        
        Alamofire.request(.GET, URLs.GET_TUTORIALS, parameters: params)
            .responseJSON { response in
                if let res = response.response {
                    if res.statusCode == 200 {
                        // response
                        if response.result.value != nil {
                        let loginInfo = LoginInfo(username: oldLoginInfo.username, json: response.result.value!)
                        Pantry.pack(loginInfo, key: Constants.PANTRY_LOGIN_INFO)
                        Pantry.pack(true, key: Constants.PANTRY_LOGIN_INFO_IS_VALID, expires: StorageExpiry.Seconds(Double(loginInfo.expiresIn)))
                        
                        GeneralFunctions.hideProgress()
                        
                        // refresh active view
                        if let viewController = UIApplication.sharedApplication().keyWindow?.rootViewController {
                            if viewController.respondsToSelector(Selector("refresh:")) {
                                viewController.performSelector(Selector("refresh:"), withObject: self)
                            }
                        }
                        }
                    } else {
                        // show error in active view
                        errorInRefresh()
                    }
                } else {
                    errorInRefresh()
                }
        }

    }
    
    private static func errorInRefresh() {
        // show error in active view
        if let viewController = UIApplication.sharedApplication().keyWindow?.rootViewController {
            if viewController.respondsToSelector(Selector("showError:")) {
                viewController.performSelector(Selector("showError:"), withObject: "خطا در به هنگام سازی توکن")
            }
        }
    }
    
}