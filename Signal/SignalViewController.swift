//
//  SignalViewController.swift
//  Signal
//
//  Created by Sina Khalili on 2/18/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class SignalViewController: UIViewController {
    
    override func viewDidLoad() {
        customizations()
    }
    
    override func willMoveToParentViewController(parent: UIViewController?) {
        customizations()
    }
    
    func customizations() {
        // customizing bar button items' fonts
        let attributes = [NSFontAttributeName:UIFont(name: Constants.FONT, size: 15)!]
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(attributes, forState: .Normal)
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: Constants.FONT, size: 16.0)! ]
    }
    
    func initWhiteNavigationBar() {
        navigationController?.navigationBar.barStyle = .Black
        navigationController?.navigationBar.barTintColor = Colors.blue
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: Constants.FONT, size: 16.0)!,
            NSForegroundColorAttributeName: UIColor.whiteColor()]
    }

    // next text field!
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField.tag>0) {
            if let nextView: UIView = view.viewWithTag(textField.tag+1) {
                nextView.becomeFirstResponder()
            } else {
                textField.resignFirstResponder()
                submit()
            }
        }
        return false;
    }
    
    // add logo to navigation bar
    func addTitleLogo() {
        let imageView = UIImageView(image: UIImage(imageLiteral: "Logo"))
        imageView.contentMode = .ScaleAspectFit

        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        imageView.frame = titleView.bounds;
        titleView.addSubview(imageView)
        
        self.navigationItem.titleView = titleView;
    }
    
    // submit on keyboard return key
    func submit() { }
    
    // progress bar

    func showProgress() {
        GeneralFunctions.showProgress()
    }
    
    func hideProgress() {
        GeneralFunctions.hideProgress()
    }
    
    // valid token
    func isTokenValid() -> Bool {
        let isTokenValid = GeneralFunctions.isTokenValid()
        if !isTokenValid {
            self.endRefreshing()
            self.endLoadingMore()
        }
        return isTokenValid
    }

    
    // refresh
    
    var isRefreshing    = false
    var isLoadingMore   = false
    var refreshControl  = UIRefreshControl()
    func initRefreshControl(tableView: UITableView, color: UIColor? = nil) {
        if color != nil {
            refreshControl.tintColor = color
        }
        refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refreshControl)
    }

    func refresh(sender:AnyObject) {
        isRefreshing = true
    }

    func endRefreshing() {
        if self.isRefreshing {
            self.isRefreshing = false
        }
        self.refreshControl.endRefreshing()
    }
    func endLoadingMore() { }
    
    // preferred status bar style
    var statusBarStyle = UIStatusBarStyle.Default
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return statusBarStyle
    }
    
}