//
//  OneTutorialCell.swift
//  Signal
//
//  Created by Sina Khalili on 2/21/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit

class OneTutorialCell: UITableViewCell {

    @IBOutlet weak var lblTitle: SignalLabel!
    @IBOutlet weak var lblDescription: SignalLabel!

    @IBOutlet var viewContainer: SignalContainerView!
    @IBOutlet var viewBehindImg: SignalContainerView!
    @IBOutlet var img: SignalImageView!

    @IBOutlet var lblCategory: SignalLabel!
    @IBOutlet var lblDateTime: UILabel!

    @IBOutlet var btnBuy: SpecialButton!
}
