//
//  SignalsViewController.swift
//  Signal
//
//  Created by Sina Khalili on 2/19/16.
//  Copyright © 2016 Omid Analyzer. All rights reserved.
//

import UIKit
import Alamofire
import ImageLoader
import Pantry

class SignalsViewController: SignalViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var topBarBackground: UIImageView!
    @IBOutlet var listHeaderBackground: UIImageView!
    @IBOutlet var listHeaderBackgroundY: NSLayoutConstraint!
    @IBOutlet var listHeaderBackgroundHeight: NSLayoutConstraint!
    @IBOutlet var lineChartView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: SignalSegmentedControl!
    @IBOutlet var indChart: UIActivityIndicatorView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet var lblContinuousSuccessSignals: SignalLabel!
    @IBOutlet var lblSuccessSignals: SignalLabel!
    @IBOutlet var lblFailedSignals: SignalLabel!
    @IBOutlet var lblSuccessRatio: SignalLabel!
    
    var db                      = DBSignals()
    var allItems: [OneSignal]   = [OneSignal]()
    var items: [OneSignal]      = [OneSignal]()
    var lastResponse            = ResponseSignals()
    var responseSignalChart     = ResponseSignalChart()
    var lineChart               = SignalLineChart()
    
    var finishedLoading = false
    
    var categoryId: Int?
    
    override func viewDidLoad() {
        
        navigationController?.navigationBar.barStyle = .Black
        
        initRefreshControl(self.tableView, color: UIColor.whiteColor())
        self.tableView.separatorStyle = .None
        scrollViewDidScroll(tableView)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 230
        
        if let chachedResponse: String = Pantry.unpack(Constants.PANTRY_CHACHED_SIGNALS) {
            // load cache
            if let json = chachedResponse.toDictionaryAsAnyObject() {
                self.lastResponse = ResponseSignals(json: json)
                if self.lastResponse.data.count > 0 {
                    self.allItems.appendContentsOf(self.lastResponse.data)
                } else {
                    self.finishedLoading = true
                }
                self.updateItems()
                refreshNeeded = true
            } else {
                downloadData(1)
            }
        } else {
            downloadData(1)
        }
        
        downloadChart()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBar.hidden = true
    }

    var refreshNeeded = false
    override func viewDidAppear(animated: Bool) {
        if refreshNeeded {
            refreshNeeded = false
            tableView.setContentOffset(CGPointMake(0, -100), animated: true)
            self.refreshControl.beginRefreshing()
            refresh(self)
        }
    }
    
    override func refresh(sender: AnyObject) {
        super.refresh(self)
        isRefreshing = true
        Alamofire.Manager.cancelRequests(URLs.GET_SIGNALS)
        downloadData(1)
    }
    
    override func endRefreshing() {
        super.endRefreshing()
        Alamofire.Manager.cancelRequests(URLs.GET_SIGNALS)
    }
    override func endLoadingMore() {
        super.endLoadingMore()
        self.indicator.stopAnimating()
        self.isLoadingMore = false
        Alamofire.Manager.cancelRequests(URLs.GET_SIGNALS)
    }
    
    @IBAction func segmentChanged(sender: AnyObject) {
        updateItems()
    }
    
    func updateItems() {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            items = allItems
            break
        case 1:
            let date = NSDate(timeIntervalSince1970: NSDate().timeIntervalSince1970-24*60*60)
            finishedLoading = filterItems(date)
            break
        case 2:
            let date = NSDate()
            finishedLoading = filterItems(date)
            break
        default:
            break
        }
        if items.count == 0 && allItems.count > 0 {
            loadMoreIfPossible()
        }
        if finishedLoading {
            endLoadingMore()
        }
        tableView.reloadData()
    }
    
    /**
     Filters items due to selected segment
     
     - parameter date: the creationDate we want to show
     
     - returns: loading finished or not
     */
    func filterItems(date: NSDate) -> Bool {
        items.removeAll()
        if allItems.count == 0 {
            return false
        }
        for i in 0...allItems.count-1 {
            if allItems[i].createdAt.isInSameDayAs(date: date) {
                items.append(allItems[i])
            } else if allItems[i].createdAt.isBeforeDay(date: date) {
                return true
            }
        }
        return false
    }
    
    func downloadData(page: Int) {
        
        WebServices.getSignals(page, categoryId: categoryId, viewController: self, indicator: indicator, view: tableView, success:  { (response) -> () in
            
            //no errors
            // response
            self.endRefreshing()
            self.endLoadingMore()
            if response != nil {
                self.lastResponse = ResponseSignals(json: response!)
                if page==1 {
                    self.allItems.removeAll()
                    // cache response
                    if self.categoryId == nil {
                        if let json = (response as! NSDictionary).toString() {
                            Pantry.pack(json, key: Constants.PANTRY_CHACHED_SIGNALS)
                        }
                    }
                }
                if self.lastResponse.data.count > 0 {
                    self.allItems.appendContentsOf(self.lastResponse.data)
                } else {
                    self.finishedLoading = true
                }
                self.updateItems()
                
                var appLastIds = AppLastIds()
                if self.items.count > 0 && appLastIds.lastSignalId < self.items[0].id {
                    appLastIds.lastSignalId = self.items[0].id
                    appLastIds.save()
                }
                
            }
            
            }, failure: { (code, response) -> () in
                // error
                super.endRefreshing()
                super.endLoadingMore()
                if(response==nil || !ResponseErrorProcessor.process(response!, view: self.tableView)) {
                    self.tableView.messageWarning("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                }
        })
        
    }
    
    func downloadChart() {
        
        WebServices.getSignalsChart(self, view: tableView, success:  { (response) -> () in
            
            // no errors
            // response
            if response != nil {
                self.responseSignalChart = ResponseSignalChart(json: response!)
                self.showChart()
            }
            
            }, failure: { (code, response) -> () in
                // error
                super.endRefreshing()
                super.endLoadingMore()
                if(response==nil || !ResponseErrorProcessor.process(response!, view: self.tableView)) {
                    self.tableView.messageWarning("\(Strings.ERROR_IN_CONNECTION)\n\(Strings.ERROR_CODE) \(code)")
                }
        })
        
    }
    
    func showChart() {
        
        lblContinuousSuccessSignals.text    = responseSignalChart.continuousSuccessSignals.persianDigits()
        lblSuccessSignals.text              = responseSignalChart.successSignals.persianDigits()
        lblFailedSignals.text               = responseSignalChart.failedSignals.persianDigits()
        lblSuccessRatio.text                = responseSignalChart.successToFailRatio.persianDigits()
        
        indChart.stopAnimating()
        
        // simple arrays
        let data: [CGFloat] = responseSignalChart.signalsEquity
        let data2: [CGFloat] = responseSignalChart.marketEquity
        
        // simple line with custom x axis labels
        var timeSlices = [String]()
        for (_, timeSlice) in responseSignalChart.timeSlices.enumerate() {
            if let timeSliceDate = timeSlice.toDateFrom_yyyy_MM_dd() {
                timeSlices.append(timeSliceDate.getDayOfWeekInPersian())
            } else {
                timeSlices.append("")
            }
        }
        
        let xLabels: [String] = timeSlices
        
        lineChart.x.grid.count = CGFloat(responseSignalChart.timeSlices.count)
        lineChart.y.grid.count = CGFloat(responseSignalChart.timeSlices.count)
        lineChart.x.labels.values = xLabels
        lineChart.addLine(data)
        lineChart.addLine(data2)
        
        lineChartView.addSubview(lineChart)
        lineChart.frame = CGRect(x: 0, y: 0, width: lineChartView.frame.width, height: lineChartView.frame.height-16)
        
    }
    
    func scrollToTop(sender: AnyObject) {
        self.tableView.setContentOffset(CGPoint.zero, animated:true)
    }
    
    // table view implementation
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if items.count == 0 && (allItems.count > 0 || finishedLoading) {
            return 1
        }
        return items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // nothing cell!
        if indexPath.row == 0 && items.count == 0 && (allItems.count > 0 || finishedLoading) {
            return tableView.dequeueReusableCellWithIdentifier("nothingCell", forIndexPath: indexPath)
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("oneSignalCell", forIndexPath: indexPath) as! OneSignalCell
        
        // showing signal
        let oneItem                     = items[indexPath.row]
        cell.lblTitle.text              = oneItem.title
        cell.lblDate.text               = oneItem.createdAt.smartDate()
        cell.lblTime.text               = oneItem.createdAt.smartTime()
        cell.btnBuy.titleLabel!.adjustsFontSizeToFitWidth = true
        cell.lblDescription.text        = oneItem.summary
        
        // buy!
        cell.btnBuy.setTitleColor(Colors.specialBlue, forState: .Normal)
        cell.btnBuy.addBorder(Colors.specialBlue)
        if oneItem.isAccessible {
            if oneItem.isReceived {
                cell.btnBuy.setTitle(Strings.DELETE, forState: .Normal)
                cell.btnBuy.setTitleColor(UIColor.redColor(), forState: .Normal)
                cell.btnBuy.addBorder(UIColor.redColor())
            } else {
                cell.btnBuy.setTitle(Strings.RECEIVE, forState: .Normal)
            }
        } else {
            cell.btnBuy.setTitle(oneItem.price.formattedPrice(), forState: .Normal)
        }
        let buyGesture              = UITapGestureRecognizer(target: self, action: "buyTap:")
        cell.btnBuy.tag             = indexPath.row
        cell.btnBuy.addGestureRecognizer(buyGesture)
        
        // is broken
        if oneItem.isBroken == 1 {
            cell.viewLimit.hidden       = false
            cell.viewLimitHeight.constant = 24
            if oneItem.isAccessible {
                cell.lblLimit.text          = Strings.SIGNAL_BROKEN
            } else {
                cell.lblLimit.text          = Strings.SIGNAL_LIMIT_REACHED
            }
        } else if oneItem.sellCounter == oneItem.limit {
            cell.viewLimit.hidden       = false
            cell.viewLimitHeight.constant = 24
            cell.lblLimit.text          = Strings.SIGNAL_LIMIT_REACHED
        } else {
            cell.viewLimit.hidden       = true
            cell.viewLimitHeight.constant = 0
        }
        
        // following
        let followingGesture        = UITapGestureRecognizer(target: self, action: "followTap:")
        cell.btnFollow.tag          = indexPath.row
        cell.btnFollow.addGestureRecognizer(followingGesture)
        if oneItem.isFollowing {
            if oneItem.isFollowInProgress {
                cell.btnFollow.setTitle("", forState: .Normal)
                cell.indFollow.color = UIColor.whiteColor()
                cell.indFollow.startAnimating()
            } else {
                cell.btnFollow.setTitle(Strings.Check, forState: .Normal)
            }
            cell.btnFollow.set(true)
        } else {
            if oneItem.isFollowInProgress {
                cell.btnFollow.setTitle("", forState: .Normal)
                cell.indFollow.color = Colors.specialBlue
                cell.indFollow.startAnimating()
            } else {
                cell.btnFollow.setTitle(Strings.FOLLOW, forState: .Normal)
            }
            cell.btnFollow.set(false)
        }
        if oneItem.isFollowInProgress {
        } else {
            cell.indFollow.stopAnimating()
        }
        
        // behaviors
        let behaviorsGesture            = UITapGestureRecognizer(target: self, action: "behaviorsTap:")
        cell.viewBehaviors.tag          = indexPath.row
        cell.viewBehaviors.addGestureRecognizer(behaviorsGesture)
        
        cell.lblGrow.text               = "\(oneItem.grow.persianDigits())٪"
        
        if oneItem.symbol.image.characters.count > 0 {
            cell.imgSymbol.load(oneItem.symbol.imageURL(), placeholder: UIImage(named: "Logo"), completionHandler: nil)
        } else {
            cell.imgSymbol.image = UIImage(named: "Logo")
        }
        cell.imgSymbol.makeRoundedCorners()
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let row = indexPath.row
        
        if row>0 && row>items.count-4 {
            loadMoreIfPossible()
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if items[indexPath.row].isAccessible {
            performSegueWithIdentifier("details", sender: indexPath.row)
        }
        
    }
    
    // topBar
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        if yOffset <= 0 {
            listHeaderBackgroundY.constant          = -20
            listHeaderBackgroundHeight.constant     = 352 - yOffset
            topBarBackground.image                  = nil
        } else {
            listHeaderBackgroundY.constant          = -20 - yOffset
            listHeaderBackgroundHeight.constant     = 352
            topBarBackground.image                  = UIImage(named: Constants.Image_Lists_Header_TopBar)
        }
    }
    
    func behaviorsTap(recognizer: UITapGestureRecognizer) {
        performSegueWithIdentifier("behaviors", sender: recognizer)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let segueIdentifier = segue.identifier {
            switch segueIdentifier {
            case "behaviors":
                (segue.destinationViewController as! SignalBehaviorsViewController).signal = items[(sender as! UITapGestureRecognizer).view!.tag]
                break
            case "details":
                (segue.destinationViewController as! SignalDetailsViewController).signal = items[sender as! Int]
                break
            default:
                break
            }
        }
    }
    
    /**
     Loads more items if exists...
     */
    func loadMoreIfPossible() {
        if !isRefreshing && !isLoadingMore && lastResponse.lastPage>lastResponse.currentPage && !finishedLoading {
            self.isLoadingMore = true
            downloadData(lastResponse.currentPage+1)
        }
    }
    
    @IBAction func categoriesPressed(sender: AnyObject) {
        ViewPresenters.openCategories(self, categoryType: .signal, categoryId: categoryId)
    }
    @IBAction func downloadsPressed(sender: AnyObject) {
        
    }
    
}
